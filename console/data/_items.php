<?php
return [
    'root' => [
        'type' => 1,
        'description' => 'Директор',
        'children' => [
            'manager',
            'admin',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администрация',
        'children' => [
            'manager',
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Гость',
    ],
];