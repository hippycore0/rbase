<?php
return [
    'root' => [
        'type' => 1,
        'description' => 'Директор',
        'children' => [
            'admin',
            'manager',
            'view',
            'call'
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'children' => [
            'manager',
            'view',
            'call'
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Менеджер',
        'children' => [
            'view',
            'call'
        ],
    ],
    'call' => [
        'type' => 1,
        'description' => 'Call-центр',
        'children' => [
            'view',
        ],
    ],
    'view' => [
        'type' => 1,
        'description' => 'Просмотр',
    ],
];