<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;
        $authManager->removeAll();
        // Create roles
        $root  = $authManager->createRole('root');
        $admin  = $authManager->createRole('admin');
        $manager = $authManager->createRole('manager');
        $call  = $authManager->createRole('call');
        $viewer  = $authManager->createRole('view');
        $guest  = $authManager->createRole('guest');

        // Create simple, based on action{$NAME} permissions
        $login  = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error  = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');
        $index  = $authManager->createPermission('index');
        $viewTable = $authManager->createPermission('viewTable');
        $createReserve = $authManager->createPermission('createReserve');
        $createClient = $authManager->createPermission('createClient');
        $createService = $authManager->createPermission('createService');
        $createPayment = $authManager->createPermission('createPayment');
        $create = $authManager->createPermission('create');
        $underCall = $authManager->createPermission('underCall');
        $print = $authManager->createPermission('print');
        $edit = $authManager->createPermission('edit');
        $editPayment = $authManager->createPermission('editPayment');
        $all = $authManager->createPermission('all');

        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($index);
        $authManager->add($viewTable);
        $authManager->add($createReserve);
        $authManager->add($createClient);
        $authManager->add($createService);
        $authManager->add($createPayment);
        $authManager->add($print);
        $authManager->add($edit);
        $authManager->add($editPayment);
        $authManager->add($all);
        $authManager->add($create);
        $authManager->add($underCall);

        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $root->ruleName  = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;
        $manager->ruleName  = $userGroupRule->name;
        $call->ruleName =   $userGroupRule->name;
        $viewer->ruleName = $userGroupRule->name;
        $guest->ruleName  = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($root);
        $authManager->add($admin);
        $authManager->add($manager);
        $authManager->add($call);
        $authManager->add($viewer);
        $authManager->add($guest);

        // Add permission-per-role in Yii::$app->authManager
        // Guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $signUp);
        $authManager->addChild($guest, $index);

        // Viewer
        $authManager->addChild($viewer, $viewTable);
        $authManager->addChild($viewer, $guest);

        // Call
        $authManager->addChild($call, $createReserve);
        $authManager->addChild($call, $createClient);
        $authManager->addChild($call, $createService);
        $authManager->addChild($call, $create);
        $authManager->addChild($call, $viewer);

        // Manager
        $authManager->addChild($manager, $createPayment);
        $authManager->addChild($manager, $print);
        $authManager->addChild($manager, $underCall);
        $authManager->addChild($manager, $call);

        // Admin
        $authManager->addChild($admin, $edit);
        $authManager->addChild($admin, $manager);

        // Root
        $authManager->addChild($root, $editPayment);
        $authManager->addChild($root, $all);
        $authManager->addChild($root, $admin);

        $authManager->assign($root, 1);
        $authManager->assign($root, 4);
        $authManager->assign($root, 12);
        $authManager->assign($root, 16);
        $authManager->assign($root, 10);
        $authManager->assign($root, 11);
        $authManager->assign($root, 13);

        $authManager->assign($admin, 5);

        $authManager->assign($manager, 14);
        $authManager->assign($manager, 6);
        $authManager->assign($manager, 17);


        $authManager->assign($call, 46);
        $authManager->assign($call, 45);
        $authManager->assign($call, 31);
        $authManager->assign($call, 20);
        $authManager->assign($call, 21);
        $authManager->assign($call, 22);
        $authManager->assign($call, 23);
        $authManager->assign($call, 24);
        $authManager->assign($call, 25);
        $authManager->assign($call, 26);
        $authManager->assign($call, 27);
        $authManager->assign($call, 28);
        $authManager->assign($call, 29);
        $authManager->assign($call, 47);
        $authManager->assign($call, 55);
        $authManager->assign($call, 32);
        $authManager->assign($call, 33);
        $authManager->assign($call, 34);
        $authManager->assign($call, 35);
        $authManager->assign($call, 36);
        $authManager->assign($call, 38);
        $authManager->assign($call, 39);
        $authManager->assign($call, 40);
        $authManager->assign($call, 41);
        $authManager->assign($call, 42);
        $authManager->assign($call, 43);
        $authManager->assign($call, 44);
        $authManager->assign($call, 48);
        $authManager->assign($call, 49);
        $authManager->assign($call, 50);
        $authManager->assign($call, 51);
        $authManager->assign($call, 54);
        $authManager->assign($call, 53);
        $authManager->assign($call, 7);
        $authManager->assign($call, 52);
        $authManager->assign($call, 30);

        $authManager->assign($viewer, 8);
        $authManager->assign($viewer, 18);
        echo 'OK';
    }
}