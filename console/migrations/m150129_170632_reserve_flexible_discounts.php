<?php

use yii\db\Schema;
use yii\db\Migration;

class m150129_170632_reserve_flexible_discounts extends Migration
{
    public function up()
    {
        $this->addColumn("{{%reserves}}", 'discount_flexible', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        $this->dropColumn("{{%reserves}}", 'discount_flexible');
        return false;
    }
}
