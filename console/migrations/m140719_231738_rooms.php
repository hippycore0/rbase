<?php

use yii\db\Schema;
use yii\db\Migration;

class m140719_231738_rooms extends Migration
{
    public function up()
    {
        $this->createTable('rooms', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
            'color' => Schema::TYPE_STRING . ' COMMENT "Цвет"',
            'base_id' => Schema::TYPE_INTEGER . ' COMMENT "База"',
            'state' => Schema::TYPE_INTEGER . ' COMMENT "Активность"',
            'price' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Цена"',
            'description' => Schema::TYPE_STRING . ' COMMENT "Описание"',
        ]);
    }

    public function down()
    {
        $this->dropTable('rooms');
    }
}
