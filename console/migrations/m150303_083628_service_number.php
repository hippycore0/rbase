<?php

use yii\db\Schema;

class m150303_083628_service_number extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn("{{%services}}", 'number', Schema::TYPE_INTEGER);
        $this->addColumn("{{%reserved_services}}", 'number', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn("{{%services}}", 'number');
        $this->dropColumn("{{%reserved_services}}", 'number');
    }
}
