<?php

use yii\db\Schema;
use yii\db\Migration;

class m140720_110715_bands extends Migration
{
    public function up()
    {
        $this->createTable('bands', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
            'summary_hours' => Schema::TYPE_INTEGER . ' COMMENT "Отыграно часов" DEFAULT 0',
            'summary_price' => Schema::TYPE_FLOAT . ' COMMENT "Потрачено денег" DEFAULT 0.00',
            'description' => Schema::TYPE_STRING . ' COMMENT "Описание"',
            'phone' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Телефон"',
            'contact' => Schema::TYPE_STRING . ' COMMENT "Контактное лицо"',
            'email' => Schema::TYPE_STRING . ' COMMENT "Email"',
            'debt' => Schema::TYPE_FLOAT . ' COMMENT "Задолжность" DEFAULT 0.00',
            'type_id' => Schema::TYPE_SMALLINT . ' NOT NULL COMMENT "Тип" DEFAULT 1',
            'director_id' => Schema::TYPE_INTEGER . ' COMMENT "Плательщик"',
            'status' => Schema::TYPE_SMALLINT . ' COMMENT "Статус"  DEFAULT 0',
        ]);
    }

    public function down()
    {
        $this->dropTable('bands');
    }
}
