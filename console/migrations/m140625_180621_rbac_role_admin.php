<?php

use yii\db\Schema;

class m140625_180621_rbac_role_admin extends \yii\db\Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $viewSchedule  = $auth->createPermission('viewSchedule');
        $addReserve  = $auth->createPermission('addReserve');
        $addClient =  $auth->createPermission('addClient');
        $doPrint = $auth->createPermission('doPrint');
        $doPayment = $auth->createPermission('doPayment');
        $editCatalog = $auth->createPermission('editCatalog');
        $viewPayment = $auth->createPermission('viewPayment');
        $beRoot = $auth->createPermission('beRoot');
        $editUsers = $auth->createPermission('editUsers');
        $editUsers->description = 'Edit users';

        $auth->add($editUsers);
        $auth->add($viewSchedule);
        $auth->add($addReserve);
        $auth->add($addClient);
        $auth->add($doPrint);
        $auth->add($doPayment);
        $auth->add($editCatalog);
        $auth->add($viewPayment);
        $auth->add($beRoot);

        $root = $auth->createRole('root');
        $admin = $auth->createRole('admin');
        $manager = $auth->createRole('manager');
        $call = $auth->createRole('call');
        $view = $auth->createRole('view');
        $client = $auth->createRole('client');

        $auth->add($root);
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($call);
        $auth->add($view);
        $auth->add($client);

        $auth->addChild($view, $viewSchedule);
        $auth->addChild($call, $view);
        $auth->addChild($call, $addReserve);
        $auth->addChild($call, $addClient);
        $auth->addChild($call, $doPrint);
        $auth->addChild($manager, $call);
        $auth->addChild($manager, $doPayment);
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $editCatalog);
        $auth->addChild($root, $admin);
        $auth->addChild($root, $viewPayment);
        $auth->addChild($root, $beRoot);
        $auth->addChild($root, $editUsers);

        $auth->assign($root, 1);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->remove($auth->getPermission('viewSchedule'));
        $auth->remove($auth->getPermission('addReserve'));
        $auth->remove($auth->getPermission('editUsers'));
        $auth->remove($auth->getPermission('addClient'));
        $auth->remove($auth->getPermission('doPrint'));
        $auth->remove($auth->getPermission('doPayment'));
        $auth->remove($auth->getPermission('editCatalog'));
        $auth->remove($auth->getPermission('viewPayment'));
        $auth->remove($auth->getPermission('beRoot'));
        $auth->remove($auth->getRole('root'));
        $auth->remove($auth->getRole('admin'));
        $auth->remove($auth->getRole('manager'));
        $auth->remove($auth->getRole('call'));
        $auth->remove($auth->getRole('view'));
        $auth->remove($auth->getRole('client'));
    }
}
