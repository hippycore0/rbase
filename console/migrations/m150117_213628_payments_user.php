<?php

use yii\db\Schema;
use yii\db\Migration;

class m150117_213628_payments_user extends Migration
{
    public function up()
    {
        $this->addColumn("{{%payments}}", 'user_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn("{{%payments}}", 'user_id');
        return false;
    }
}
