<?php

use yii\db\Schema;
use yii\db\Migration;

class m140720_195356_reserves extends Migration
{
    public function up()
    {
        $this->createTable('reserves', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
            'price' => Schema::TYPE_FLOAT . ' COMMENT "Цена" DEFAULT 0.00',
            'description' => Schema::TYPE_STRING . ' COMMENT "Описание"',
            'units' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Ед. измерения" DEFAULT "1 шт./час"',
            'order' => Schema::TYPE_INTEGER . ' COMMENT "Порядок"',
            'base_id' => Schema::TYPE_INTEGER . ' COMMENT "Задолжность" DEFAULT 0.00'
        ]);
    }

    public function down()
    {
        echo "m140720_195356_reserves cannot be reverted.\n";

        return false;
    }
}
