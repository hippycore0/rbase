<?php

use yii\db\Schema;

class m140625_132140_password_reset extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", 'first_reset_request', Schema::TYPE_INTEGER);
        $this->addColumn("{{%user}}", 'reset_request_count', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn("{{%user}}", 'first_reset_request');
        $this->dropColumn("{{%user}}", 'reset_request_count');
    }
}
