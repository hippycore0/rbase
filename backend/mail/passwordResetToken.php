<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user backend\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

Здравствуйте <?= Html::encode($user->username) ?>!

Для сброса пароля, перейдите по ссылке ниже:

<?= Html::a(Html::encode($resetLink), $resetLink) ?>
