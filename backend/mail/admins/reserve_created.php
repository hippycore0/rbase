<?
/* @var $model app\modules\reserves\models\Reserve  */
?>
<h2>Здравствуйте, <?=$model->band->title?>!</h2>
<p>Вы забронировали время.</p>
<?if($model->getWeeks() > 1):?>
    <p>Дата и время:</p>
    <?foreach($model->getSchedule()->all() as $key=>$schedule):?>
        <?$reserved[$schedule->id] = date('d.m',$schedule->timestamp)?>
    <?endforeach;?>
    <?$days = array_unique($reserved);?>
    <?foreach($days as $schedule_id => $day):?>
        <?=$day?>., с <?=$model->start_hour?>:00 до <?=$model->start_hour+$model->hours?>:00<br />
    <?endforeach;?>
<?else:?>
    <p>Дата и время: <?=date('d.m.Y',strtotime($model->start))?> г., с <?=$model->start_hour?>:00 до <?=$model->start_hour+$model->hours?>:00</p>
<?endif;?>
<p>Репетиционный комплекс: <?=$model->base->title?></p>
<p>Комната: <?=$model->room->title?></p>
<p>Регистратор: <?=\backend\modules\users\models\User::findOne($model->user_create)->username?></p>
<hr />

<p style="font-size: 11px">
    Если Вы нашли не соответствие заявленному резерву, просим перезвонить для корректировки резерва: 8 (495) 504-39-72
</p>
<p style="font-size: 11px">Спасибо за пользование услугами Творческого Объединения Легенда!</p>