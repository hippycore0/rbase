<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <div style=" font-family: Tahoma, Verdana, Arial, sans-serif;  margin: 50px auto; max-width: 600px;text-align: right">
        <div style="background: #EFEFEF; color: #606060; border-radius: 5px; padding: 50px; text-align: left">
            <?= $content ?>
            <div style="text-align: center; font-style: italic; font-size:11px; margin-bottom: -20px;"><a href="http://www.legendbase.ru/" style="color: inherit">www.legendbase.ru</a></div>
        </div>

    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
