<?php

namespace backend\modules\reports;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\reports\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
