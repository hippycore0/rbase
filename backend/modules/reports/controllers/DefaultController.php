<?php

namespace backend\modules\reports\controllers;

use app\components\AjaxController;
use Yii;
class DefaultController extends AjaxController
{
    public function actionIndex()
    {
        $this->enableCsrfValidation = false;
        $params = Yii::$app->request->post();

        if(isset($params['start_date']) && $params['start_date'] > 0):
            Yii::$app->session->set('report_start',$params['start_date']);
        endif;
        if(Yii::$app->session->has('report_start')){
            $start = new \Datetime(Yii::$app->session->get('report_start'));
        }else{
            $d = new \Datetime();
            $d->setTime(0,0,0);
            $d->modify('-6 days');
            $start = $d;
        }

        if(isset($params['end_date']) && $params['end_date'] > 0):
            Yii::$app->session->set('report_end',$params['end_date']);
        endif;
        if(Yii::$app->session->has('report_end')){
            $end = new \Datetime(Yii::$app->session->get('report_end'));
        }else{
            $d = new \Datetime();
            $d->setTime(0,0,0);
            $d->modify('+2 day');
            $end = $d;
        }
        $xAxis = [
            'categories'=>[]
        ];
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach ( $period as $dt ){
            array_push($xAxis['categories'],$dt->format('d.m.Y'));
        }
        $yAxis = [
            'title'=>[
                'text'=>'Часы'
            ]
        ];
        $series = [];
        foreach(\app\modules\rooms\models\Room::find()->all() as $room){
            $s['name'] = $room->title;
            $s['id'] = $room->id;
            $s['data'] = $room->getSumHoursByPeriod($start,$end);
            $s['pointStart'] = 0;
            array_push($series,$s);
        }
        $params = [
            'xAxis'=>$xAxis,
            'yAxis'=>$yAxis,
            'series'=>$series,
            'start'=>$start,
            'end'=>$end
        ];
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_chart',$params);
        } else {
            return $this->render('index',$params);
        }
    }
}
