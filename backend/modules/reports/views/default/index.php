<?

\backend\assets\ReportAsset::register($this);
?>
    <?\yii\bootstrap\ActiveForm::begin([
        'id'=>'reportFilter',
        'action'=>'/report',
        'method'=>'POST'
    ]);?>
    <div class="row">
        <div class="col-xs-3">
            <?=
            \yii\jui\DatePicker::widget([
                'name' => 'start_date',
                'language' => 'ru',
                'value' => $start,
                'dateFormat' => 'dd.MM.yyyy',
                'options' => ['placeholder' => 'Начальная дата', 'class' => 'form-control', 'size' => '10']
            ]) ?>
        </div>
        <div class="col-xs-3">
            <?=
            \yii\jui\DatePicker::widget([
                'name' => 'end_date',
                'language' => 'ru',
                'value' => $end,
                'dateFormat' => 'dd.MM.yyyy',
                'options' => ['placeholder' => 'Конечная дата', 'class' => 'form-control', 'size' => '10']
            ]) ?>
        </div>
        <div class="col-xs-3">
            <?= \yii\helpers\Html::dropdownList('stat', Yii::$app->session->get('report_stat'),
                [
                    0 => 'Часы (сумм)',
                    1 => 'Часы (сред)',
                    2 => 'Приход (сумм)',
                    3 => 'Приход (сред)',
                ],
                ['class' => 'form-control']) ?>
        </div>
        <div class="col-xs-3">
            <?= \yii\helpers\Html::dropdownList('stat', Yii::$app->session->get('report_stat'),
                [
                    0 => 'день ',
                    1 => 'неделя ',
                    2 => 'месяц',
                    3 => 'квартал',
                    4 => 'год',
                    5 => 'все время',
                ],
                ['class' => 'form-control']) ?>
        </div>
    </div>
    <br />
    <?\yii\bootstrap\ActiveForm::end();?>
<div id="charts">
    <?=$this->render('_chart',['xAxis'=>$xAxis,'yAxis'=>$yAxis,'series'=>$series])?>
</div>