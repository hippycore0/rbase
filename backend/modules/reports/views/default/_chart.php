<?
use dosamigos\highcharts\HighCharts;
?>
<?= HighCharts::widget([
    'clientOptions' => [
        'type' => 'line',
        'title' => [
            'text' => 'Отчетность'
        ],
        'xAxis' => $xAxis,
        'yAxis' => $yAxis,
        'series' => $series
    ],

]);
?>