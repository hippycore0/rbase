<?php

namespace backend\modules\api\controllers;

use yii\rest\ActiveController;
use yii\web\Controller;
use app\modules\reserves\models\Reserve;
class ReservesController extends ActiveController
{
    public $modelClass = 'app\modules\reserves\models\Reserve';

    public function actionCheck(){
        $model = new Reserve();
        $params['Reserve'] = \Yii::$app->request->post();

        if ($model->load($params)) {

            if(empty($model->end)) {
                $model->end = $model->start;
            }
            if($model->start_hour == 0 && $model->hours == 6){
                $model->is_night = true;
            }
            if($model->is_night){
                $model->start_hour = 0;
                $model->hours = 6;
            }

            if(!$model->band_id){
                $model->band_id = 3433;
            }
            $model->status = 'ready';
            $model->summary_payment = 0;


            if($model->validate()){
                return ['status'=>'success'];
            }else{
                return ['status'=>'fail','errors'=>$model->getErrors(),'params'=>\Yii::$app->request->post()];
            }
        }else{
            return ['status'=>'fail','errors'=>$model->getErrors(),'params'=>\Yii::$app->request->post()];
        }
    }
    public function actionReserve(){
        $model = new Reserve();
        $params['Reserve'] = \Yii::$app->request->post();

        if ($model->load($params)) {

            if(empty($model->end)) {
                $model->end = $model->start;
            }
            if($model->is_night){
                $model->start_hour = 0;
                $model->hours = 6;
            }

            if(!$model->band_id){
                $model->band_id = 3433;
            }
            $model->status = 'ready';
            $model->summary_payment = 0;

            if($model->save()){
                return ['status'=>'success'];
            }else{
                return ['status'=>'fail','errors'=>$model->getErrors(),'params'=>\Yii::$app->request->post()];
            }
        }else{
            return $model->getErrors();
        }
    }

}
