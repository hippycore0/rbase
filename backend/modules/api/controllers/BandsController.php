<?php

namespace backend\modules\api\controllers;

use yii\rest\ActiveController;
use yii\web\Controller;
class BandsController extends ActiveController
{
    public $modelClass = 'app\modules\reserves\models\Bands';
}
