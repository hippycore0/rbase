<?php

namespace backend\modules\api\controllers;

use app\modules\rooms\models\Room;
use yii\rest\ActiveController;
use yii\web\Controller;
use yii\filters\VerbFilter;
class RoomsController extends ActiveController
{
    public $modelClass = 'app\modules\rooms\models\Room';

    function actionGet(){
        return Room::find()->where('id !=10 AND id !=12')->all();
    }

}
