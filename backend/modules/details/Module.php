<?php

namespace app\modules\details;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\details\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
