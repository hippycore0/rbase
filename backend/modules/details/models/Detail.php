<?php

namespace app\modules\details\models;

use Yii;

/**
 * This is the model class for table "details".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $value
 */
class Detail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'alias', 'value'], 'required'],
            [['title', 'alias', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'value' => 'Value',
        ];
    }
}
