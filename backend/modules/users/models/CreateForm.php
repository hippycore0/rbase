<?php
namespace backend\modules\users\models;

use yii\base\Model;
use Yii;

/**
 * Create user form
 */
class CreateForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $status;
    public $roles;

    protected $_all_roles;

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        $auth = Yii::$app->authManager;
        $this->_all_roles = $auth->getRoles();
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username', 'email', 'status', 'password'], 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\backend\modules\users\models\User', 'message' => 'Указанный Email адрес уже используется'],

            ['password', 'string', 'min' => 6],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED]],
            ['roles', 'rolesValidator'],
        ];
    }

    /**
     * Validate roles
     *
     * @param $attribute
     * @param $params
     */
    public function rolesValidator($attribute, $params)
    {
        if ($this->$attribute && is_array($this->$attribute)) {
            foreach ($this->$attribute as $val) {
                if (!isset($this->_all_roles[$val])) {
                    $this->addError($attribute, 'Роль "' . $val . '" не существует');
                    break;
                }
            }
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            $this->processRoles($user->id);
            return $user;
        }

        return null;
    }

    /**
     * Apply new rules
     *
     * @param $user_id
     */
    protected function processRoles($user_id)
    {
        $auth = Yii::$app->authManager;
        $user_roles = $auth->getRolesByUser($user_id);
        if (!is_array($this->roles)){
            $this->roles = array();
        }
        foreach ($this->roles as $r) {
            if (!isset($user_roles[$r])) {
                $auth->assign($this->_all_roles[$r], $user_id);
            }
        }

        foreach ($user_roles as $k => $v) {
            if (!in_array($k, $this->roles))
            $auth->revoke($v, $user_id);
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'username' => 'Имя',
            'status' => 'Статус',
            'roles' => 'Роли',
        ];
    }
}
