<?php
namespace backend\modules\users\models;

use Yii;

/**
 * Update user form
 */
class UpdateForm extends CreateForm
{
    private $user;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['profile'] = ['username', 'password'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        $this->username = $this->user->username;
        $this->email = $this->user->email;

        $auth = Yii::$app->authManager;
        $this->roles = array_keys($auth->getRolesByUser($this->user->id));
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\backend\modules\users\models\User', 'filter' => ['not in', 'id', [$this->user->id]], 'message' => 'Указанный Email адрес уже используется'],

            ['password', 'string', 'min' => 6],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED]],

            ['roles', 'rolesValidator'],
        ];
    }

    /**
     * Update user info.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save()
    {
        if ($this->validate()) {
            $this->user->username = $this->username;
            $this->user->email = $this->email;
            if ($this->password)
                $this->user->setPassword($this->password);
            $this->user->save();
            $this->processRoles($this->user->id);
            return true;
        }

        return false;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
