<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\users\models\User;
use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?php if ($model->getScenario() != 'profile'): ?>
        <?=
        $form->field($model, 'roles')->widget(MultiSelect::className(),
            [
                'data' => [
                    User::ROLE_ROOT => 'Дирекция',
                    User::ROLE_ADMIN => 'Руководство',
                    User::ROLE_MANAGER => 'Администрация',
                    User::ROLE_CALL => 'Колцентр',
                    User::ROLE_VIEW => 'Просмотр',
                ],
                'options' => [
                    'multiple' => true,
                ]
            ])?>
        <?=
        $form->field($model, 'status')->dropDownList([
            User::STATUS_ACTIVE => 'Активен',
            User::STATUS_DELETED => 'Не активен',
        ])?>
    <? endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
