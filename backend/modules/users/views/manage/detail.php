<h2>Информация</h2>
<?= \yii\widgets\DetailView::widget([
    'model' => $model
]) ?>
<h2>История действий</h2>
<?=$this->render('@backend/views/history/_widget',['model'=>\app\models\History::find()->where(['user_id'=>$model->id])->orderBy(['create_at'=>SORT_DESC])->all()])?>