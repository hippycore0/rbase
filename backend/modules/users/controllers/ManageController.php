<?php

namespace backend\modules\users\controllers;

use app\components\AjaxController;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\modules\users\models\User;
use backend\modules\users\models\UserSearch;
use backend\modules\users\models\UpdateForm;
use backend\modules\users\models\CreateForm;
use yii\web\ForbiddenHttpException;

/**
 * ManageController implements the CRUD actions for User model.
 */
class ManageController extends AjaxController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('all')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('all')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new CreateForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('all')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new UpdateForm($this->findModel($id));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfile()
    {
        if (!Yii::$app->user->can('all')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new UpdateForm($this->findModel(Yii::$app->user->id));
        $model->scenario = 'profile';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goHome();
        } else {
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
    }

    public function actionView($id){
        if (!Yii::$app->user->can('underCall')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('all')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
