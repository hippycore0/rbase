<?php

namespace app\modules\services\controllers;

use app\components\AjaxController;
use app\modules\rooms\models\Room;
use Yii;
use app\modules\services\models\Service;
use app\modules\services\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\db\Query;
use yii\helpers\Json;
/**
 * DefaultController implements the CRUD actions for Service model.
 */
class DefaultController extends AjaxController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('createService')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionList()
    {
        if (!Yii::$app->user->can('createService')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'models' => Service::find()->all(),
        ]);
    }

    /**
     * Displays a single Service model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('createService')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('underCall')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new Service();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('edit')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('edit')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionJson($room_id = null) {
        $out = ['more' => false];
        if (!is_null($room_id)) {
            $base_id = Room::findOne($room_id)->base_id;
            $query = new Query;
            $query->select('id, title AS text')
                ->from('services')
                ->where('base_id = ' . $base_id .'');
            $command = $query->createCommand();
            $data = $command->queryAll();
            if(count($data) > 0 ){
                $out['results'] = array_values($data);
            }else{
                $out['results'] = array_values([['id' => 0, 'text' => 'На данной базе услуг не найдено']]);
            }
        }
        else {
            $out['results'] = array_values([['id' => 0, 'text' => 'Нe указана комната']]);
        }
        echo Json::encode($out);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
