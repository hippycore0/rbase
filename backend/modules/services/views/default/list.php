<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\services\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доп. услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-bordered" id="serviceList">
        <thead>
            <tr>
                <th>№</th>
                <th>Наименование</th>
                <th>Цена</th>
                <th>База</th>
                <th>Кол-во</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?foreach($models as $model):?>
            <tr data-service_id="<?=$model->id?>" class="service_list_item">
                <td><?=$model->id?></td>
                <td><?=$model->title?></td>
                <td><?=$model->price?></td>
                <td><?=$model->getBase()->one()->title?></td>
                <td><?=$model->number?><!--input type="number" name="service_number" style="width:50px;" max="<?=$model->number?>" value="<?=$model->number?>" min="1"/--></td>
            </tr>
            <?endforeach;?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).on('click','#serviceList .service_list_item:not(.checked)',function(){
        addService($(this).data('service_id'));
        $(this).addClass('checked');
    }).on('click','#serviceList .service_list_item.checked',function(){
        removeService($(this).data('service_id'));
        $(this).removeClass('checked');
    });
    function removeService(id){
        var select = $('#reserve-new_services');
        var val = select.val();
        val.splice(val.indexOf(id),1);
        console.log(val);
        select.val(val).trigger("change");
        return false;
    }
    function addService(id){
        var select = $('#reserve-new_services');
        var val = select.val();
        var array = [];
        if(val != null){
            array = $.map(select.val(), function(value, index) {
                return [value];
            });
        }
        array.push(id);
        select.val(array).trigger("change");
        return false;
    }
</script>
