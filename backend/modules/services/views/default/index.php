<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\services\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доп. услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?if(Yii::$app->user->can('underCall')):?>
        <?= Html::a('Создать услуги', ['create'], ['class' => 'btn btn-success']) ?>
        <?endif;?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => '№',
                'value' => function ($model) {
                    return $model->id;
                }
            ],
            'title',
            [
                'attribute' => 'price',
                'value' => function ($model) {
                    return $model->price.' '.$model->units;
                }
            ],
            'number',
            [
                'attribute' => 'base_id',
                'value' => function ($model) {
                    return $model->base->title;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
