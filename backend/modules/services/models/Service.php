<?php

namespace app\modules\services\models;

use app\modules\rooms\models\Base;
use Yii;
use app\modules\reserves\models\Reserve;

/**
 * This is the model class for table "services".
 *
 * @property string $id
 * @property string $title
 * @property integer $price
 * @property string $units
 * @property integer $order
 * @property integer $base_id
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'base_id', 'number'], 'required'],
            [['price', 'base_id', 'number'], 'integer'],
            [['units','icon'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'price' => 'Цена',
            'units' => 'Ед. изм.',
            'base_id' => 'База',
            'icon' => 'Значок',
            'number' => 'Колличество'
        ];
    }

    public function getBase(){
        return $this->hasOne('app\modules\rooms\models\Base', array('id' => 'base_id'));
    }

    public function getReserves(){
        return $this->hasMany(Reserve::className(),array('id' => 'reserve_id'))
            ->viaTable('reserved_services',['service_id'=>'id'])->where(['status'=>'ready']);
    }

    public static function getForList(){
        $arr = \app\modules\services\models\Service::find()->orderBy('base_id')->asArray()->all();
        foreach($arr as $key => $service){
            $arr[$key]['title'].= ' :: '.Base::findOne($service['base_id'])->title;
        }
        return $arr;
    }

    /*Возвращает массив timestamps когда занята эта услуга*/
    public function getTimestamps($exclude = false){
        $timestamps = [];
        $query = $this->getReserves();
        if($exclude > 0){
            $query = $query->where('id != :id',['id'=>$exclude])->andWhere(['reserves.status'=>'ready']);
        }
        foreach($query->with('schedule')->asArray()->all() as $reserve){
            foreach($reserve['schedule'] as $schedule){
                $timestamps[] = $schedule['timestamp'];
            }
        }
        return $timestamps;
    }

    public function isAvailable($reserve){
        //$inersection = count(array_intersect($reserve->getTimestamps(),$this->getTimestamps($reserve->id)));
        $reserveTimestamps = $reserve->getTimestamps();
        $serviceTimestamps = $this->getTimestamps($reserve->id);
        $array = array_count_values($serviceTimestamps);
        $result = 0;
        $this->number=$this->number == 0?1:$this->number;
        foreach($reserveTimestamps as $timestamp){
            if(isset($array[$timestamp]) && $array[$timestamp] >= $this->number){
                $result += 1;
            }
        }
        if($result > 0){
            return false;
        }else{
            return true;
        }

    }
}
