<?
/**
 * @var $models app\modules\payments\models\Payment
 */
use yii\data\ActiveDataProvider;
use app\modules\payments\models\PaymentSearch;
use yii\grid\GridView;
use yii\helpers\Html;
$searchModel = new PaymentSearch();
$dataProvider = new ActiveDataProvider([
    'query' => $models
]);
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => '№',
            'value' => function ($model) {
                return $model->id;
            }
        ],
        'value',
        [
            'attribute' => 'user_create',
            'label' => 'Регистратор',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model->user->username,['/users/manage/view','id'=>$model->user_id]);
            }
        ],
        'date',

        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'delete') {
                    $url = '/payments/default/delete?id=' . $model->id;
                    return $url;
                }
                if ($action === 'update') {
                    $url = '/payments/default/update?id=' . $model->id;
                    return $url;
                }
                if ($action === 'view') {
                    $url = '/payments/default/view?id=' . $model->id;
                    return $url;
                }
            }
        ],
    ],
]); ?>