<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\modules\payments\models\Payment */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reserve_id')->textInput(['maxlength' => 11,'disable'=>'disable']) ?>

    <?= $form->field($model, 'band_id')->textInput(['maxlength' => 11,'disable'=>'disable']) ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'date')->textInput()->widget(DatePicker::classname(),[
        'name'=>'date',
        'language'=>'ru',
        'options'=>['placeholder' => 'Начальная дата','class'=>'form-control']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Оплатить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
