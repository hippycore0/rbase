<link rel="stylesheet" type="text/css" href="/css/cachememo.css">
<div style="width: 65%; float:left; text-align: left">
    Общество с ограниченной ответственностью "Грог",<Br />
    ИНН\КПП 7733150546\773301001<br />
    Юр. адрес: 101111 г. Москва, ул. Квака, д.1, офис 3<br />
    Факт. адрес: 101111 г. Москва, ул. Квака, д.1, офис 3<br />
    р/сч. 30301810500001000001 в ОАО "СБЕРБАНК РОССИИ" Г. МОСКВА        </div>
<div style="width: 35%; float:left; text-align: right">
    09 октября 2014 г.        </div>
<Br />
<h1 style="font-size: 2.2em; padding-top: 40px; border: 0; text-align: center; text-transform: uppercase; clear: both;">Товарный чек</h1>
<table class="bordered">
    <thead>
    <tr>
        <th>Наименование товара</th>
        <th>Ед. изм.</th>
        <th>Кол-во</th>
        <th>Цена</th>
        <th>Сумма</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td >Аренда комнаты</td>
        <td>час</td>
        <td align="right">3</td>
        <td align="right">267.00</td>
        <td align="right">801.00</td>
    </tr>
    </tbody>
</table>
<br />
<table>
    <tr>
        <td width="50%">
            <table class="fields">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="label">
                    <td>подпись</td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table class="fields">
                <tr>
                    <td>восемьсот один рубль 00 копеек</td>
                </tr>
                <tr class="label">
                    <td>сумма прописью</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</div>
