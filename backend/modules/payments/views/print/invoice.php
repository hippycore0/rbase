<div class="print_div">

    <div style="width:450px; padding-right:5px; border:solid #333; border-width:0 1px 0 0; float:left;">

        <div style="text-align:right; margin:0 0 5px 0;">
            <b>Унифицированная форма № КО-1</b><br>
            Утверждена постановлением Госкомстата России от 18.08.98 г. № 88
        </div>

        <table class="tntable" cellpadding="0" cellspacing="0" border="0" style="font-size:9pt;">
            <colgroup>
                <col>
                <col style="width:110px;">
                <col style="width:90px;">
            </colgroup>
            <tbody><tr>
                <td rowspan="4" class="rep-brd" style="border-width:0 0 1px 0; font-size:11pt; font-weight:bold;">Грог</td>
                <td></td>
                <td class="rep-brd align-center" style="border-width:1px 1px 0 1px;">Коды</td>
            </tr>
            <tr>
                <td class="align-right">Форма по ОКУД</td>
                <td class="rep-brd align-center" style="border-width:1px 1px 0 1px;">0310001</td>
            </tr>
            <tr>
                <td class="align-right">по ОКПО</td>
                <td class="rep-brd align-center" style="border-width:1px 1px 0 1px;">80881960</td>
            </tr>
            <tr>
                <td></td>
                <td class="rep-brd align-center" style="border-width:1px;">&nbsp;</td>
            </tr>
            </tbody></table>

        <table class="tntable" cellpadding="0" cellspacing="0" border="0" style="font-size:9pt; margin-top:5px;">
            <colgroup>
                <col>
                <col style="width:110px;">
                <col style="width:90px;">
            </colgroup>
            <tbody><tr>
                <td class="align-right" rowspan="2" style="font-size:10pt; padding:0 10px 5px 0; font-weight:bold;">ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР</td>
                <td class="rep-brd align-center" style="border-width:1px 0 0 1px;">Номер<br>документа</td>
                <td class="rep-brd align-center" style="border-width:1px 1px 0 1px;">Дата<br>составления</td>
            </tr>
            <tr>
                <td class="rep-brd align-center" style="border-width:2px 0 2px 2px;">27</td>
                <td class="rep-brd align-center" style="border-width:2px;">09.10.2014</td>
            </tr>
            </tbody></table>

        <table class="tntable" cellpadding="0" cellspacing="0" border="0" style="font-size:9pt; margin-top:10px;">
            <colgroup>
                <col style="width:50px;">
                <col style="width:20px;">
                <col style="width:50px;">
                <col style="width:50px;">
                <col style="width:50px;">
                <col>
                <col style="width:50px;">
                <col style="width:20px;">
            </colgroup>
            <tbody><tr>
                <td rowspan="2" class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">Дебет</td>
                <td rowspan="2" class="rep-brd align-center" style="border-width:1px 0 0 1px;">&nbsp;</td>
                <td colspan="3" class="rep-brd align-center" style="border-width:1px 0 0 1px;">Кредит</td>
                <td rowspan="2" class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">Сумма, руб. коп.</td>
                <td rowspan="2" class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">Код целевого назна-<br>чения</td>
                <td rowspan="2" class="rep-brd align-center" style="border-width:1px 1px 0 1px;">&nbsp;</td>
            </tr>
            <tr>
                <td class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">код струк-<br>турного подразде-<br>ления</td>
                <td class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">коррес-<br>понди-<br>рующий счет, субсчет</td>
                <td class="rep-brd align-center v-mid" style="border-width:1px 0 0 1px;">код аналити-<br>ческого учета</td>
            </tr>
            <tr>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;">50</td>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;">&nbsp;</td>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;">--------</td>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;">71</td>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;">-----</td>
                <td class="rep-brd align-right" style="border-width:1px 0 1px 1px; font-size:11pt;">120,00</td>
                <td class="rep-brd align-center" style="border-width:1px 0 1px 1px; font-size:11pt;"></td>
                <td class="rep-brd align-center" style="border-width:1px 1px 1px 1px; font-size:11pt;">&nbsp;</td>
            </tr>
            </tbody></table>

        <div style="margin:10px 0 0 0; line-height:125%;">
            Принято от: Коська В.А.
            <br>Основание: Возврат неиспользованного аванса
            <br>Сумма: сто двадцать руб. 00 коп.
            <br>В том числе НДС 21 руб. 60 коп.
            <br>Приложение: авансовый отчет №12 от 15 августа
        </div>

        <div style="position:relative; margin:20px 0 0 0;">
            <div id="stamp_p">&nbsp;</div>
            <div id="sign1_p">&nbsp;</div>
            <div id="sign2_p">&nbsp;</div>
            <div id="sign3_p">&nbsp;</div>
            <div id="sign4_p">&nbsp;</div>

            <table class="tntable" cellpadding="0" cellspacing="0" border="0" style="font-size:11pt;">
                <colgroup>
                    <col>
                    <col style="width:20px;">
                    <col style="width:100px;">
                    <col style="width:20px;">
                    <col style="width:180px;">
                </colgroup>
                <tbody><tr>
                    <td class="align-left">Главный бухгалтер</td>
                    <td></td>
                    <td class="rep-brd align-center" style="border-width:0 0 1px 0;"></td>
                    <td></td>
                    <td class="rep-brd align-center" style="border-width:0 0 1px 0;">Коська В.А.</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="align-center" style="padding:0; font-size:7pt; color:#777;">(подпись)</td>
                    <td></td>
                    <td class="align-center" style="padding:0; font-size:7pt; color:#777;">(расшифровка подписи)</td>
                </tr>
                <tr>
                    <td colspan="5" style="height:10px;"></td>
                </tr>
                <tr>
                    <td colspan="2" class="align-left">Получил кассир</td>
                    <td class="rep-brd align-center" style="border-width:0 0 1px 0;"></td>
                    <td></td>
                    <td class="rep-brd align-center" style="border-width:0 0 1px 0;">Коська В.А.</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="align-center" style="padding:0; font-size:7pt; color:#777;">(подпись)</td>
                    <td></td>
                    <td class="align-center" style="padding:0; font-size:7pt; color:#777;">(расшифровка подписи)</td>
                </tr>
                </tbody></table>
        </div>
    </div>
    <div style="width:190px; margin-left:5px; padding-left:5px; border:solid #333; border-width:0 0 0 1px; float:left;">
        <div style="font-size:10pt; text-align:center; line-height:125%;"><b><u>Грог</u></b></div>
        <div style="margin:10px 0 0 0; font-size:10pt; text-align:center; line-height:125%;"><b>КВИТАНЦИЯ</b></div>
        <div style="margin:10px 0 0 0; font-size:10pt; text-align:center; line-height:125%;">к приходному кассовому ордеру<br>№ 27 от </div>
        <div style="margin:10px 0 0 0; line-height:125%;">
            Принято от Коська В.А.
            Основание: Возврат неиспользованного аванса
            <br>Сумма: 120 руб. 00 коп.
            (сто двадцать руб. 00 коп.)
            <br>В том числе В том числе НДС 21 руб. 60 коп.
        </div>
        <div style="margin:50px 0 0 0; text-align:right;">
            <b></b>
        </div>
        <div style="margin:10px 0 0 0;">
            <b>М. П. (штампа)</b>
        </div>

        <div style="margin:10px 0 0 0; border:solid #333; border-width:0 0 1px 0; padding:0 0 1px 0;">
            <b>Главный бухгалтер</b>
        </div>
        <div class="align-center" style="margin:10px 0 0 0; border:solid #333; border-width:0 0 1px 0; padding:0 0 1px 0;">
            Коська В.А.
        </div>
        <div class="align-center" style="padding:0; font-size:7pt; color:#777;">
            (расшифровка подписи)
        </div>
        <div style="margin:10px 0 0 0; border:solid #333; border-width:0 0 1px 0; padding:0 0 1px 0;">
            <b>Кассир</b>
        </div>
        <div class="align-center" style="margin:10px 0 0 0; border:solid #333; border-width:0 0 1px 0; padding:0 0 1px 0;">
            Коська В.А.
        </div>
        <div class="align-center" style="padding:0; font-size:7pt; color:#777;">
            (расшифровка подписи)
        </div>
    </div>
    <div class="clr"></div>
</div>