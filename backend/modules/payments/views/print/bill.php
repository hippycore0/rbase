<div class="print_div">
<div id="hdr_r">
    <div style="text-align:right; font-size:15pt; font-weight:bold;">
        ООО Спецстрой
    </div>
    <div style="margin:5px 0 0 0; text-align:right; font-size:10pt; padding:0 0 5px 0; line-height:125%;">
        Вельск, Пушкинская 14<br>ИНН/КПП 1124577721 / 112486978<br>Тел. +7 475 14-125-41
    </div>
</div>
<div class="clr"></div>

<div style="margin:2px 0 0 0; height:5px; font-size:10pt; border:solid #555555; border-width:0 0 1px 0;"></div>
<div style="margin:2px 0 0 0; height:5px; font-size:10pt; border:solid #555555; border-width:2px 0 0 0;"></div>

<div style="margin:10px 0 0 0; font-size:10pt; font-weight:bold; text-align:center;">
    ООО Спецстрой
</div>

<div style="margin:10px 0 0 0;">
    <table class="rep-table">
        <colgroup>
            <col style="width: 50%;">
            <col style="width: 50%;">
        </colgroup>
        <tbody><tr>
            <td class="rep-cell rep-border align-left" valign="top" style="vertical-align:top;">
                <div class="rep-text">ООО Спецстрой
                    <br>Адрес: Вельск, Пушкинская 14
                    <br>ИНН/КПП 1124577721 / 112486978
                    <br>Расчетный счет 11107560605460557 в РКЦ ВЕЛЬСК, ВЕЛЬСК,
                    <br>Кор. счет 114283741942545
                    <br>БИК 041111000<br>Тел. +7 475 14-125-41<br>e-mail: org@promsystem.ru</div>
            </td>
            <td class="rep-cell rep-border align-left" valign="top" style="vertical-align:top;">
                <div class="rep-text">ООО "Стройтехнологии"
                    <br>Адрес: 654005, г.Новокузнецк, ул. Орджоникидзе, 21
                    <br>ИНН/КПП 4217140025 / -
                    <br>Расчетный счет 40702810600990006709 в АКБ НБМ ОАО г. Новокузнецк
                    <br>Кор. счет 30101810700000000705
                    <br>БИК 043209705</div>
            </td>
        </tr>
        </tbody></table>
</div>
<div class="schet_hdr">Счет № 15 от 01.02.2014</div>
<table class="rep-table">
    <colgroup>
        <col style="width:25px;">
        <col>
        <col style="width:30px;">
        <col style="width:50px;">
        <col style="width:70px;">
        <col style="width:80px;">
        <col style="width:40px;">
        <col style="width:50px;">
        <col style="width:100px;">
    </colgroup>
    <thead>
    <tr>
        <th class="rep-cell rep-border">
            <div class="rep-text">№</div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Наименование товара (услуги)</div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Ед.<br>изм.</div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Коли-<br>чество<br>(объем)</div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Цена без <nobr>НДС</nobr></div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Сумма без <nobr>НДС</nobr></div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr"><nobr>НДС</nobr></div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Сумма <nobr>НДС</nobr></div>
        </th>
        <th class="rep-cell rep-border">
            <div class="rep-text wpbr">Всего с <nobr>НДС</nobr>, руб.</div>
        </th>
    </tr>
    </thead>
    <tbody><tr>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">1</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-left"><span>Промышленный станок СМП-1</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">шт</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">2</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">125 000,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">250 000,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">0%</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">0,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">250 000,00</span></div>
        </td>
    </tr>
    <tr>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">2</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-left"><span>Вибрационная установка МТВ КТи</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">шт</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">5</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">7 850,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">39 250,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">0%</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">0,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">39 250,00</span></div>
        </td>
    </tr>
    <tr>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">3</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-left"><span>Система пусковой установки СПУ-7</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">шт</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">8</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">4 500,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">36 000,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text"><span class="nobr">0%</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">0,00</span></div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right"><span class="nobr">36 000,00</span></div>
        </td>
    </tr>
    <tr>
        <td class="rep-cell" colspan="8">
            <div class="rep-text align-right">
                <strong>Всего к оплате:</strong>
            </div>
        </td>
        <td class="rep-cell rep-border">
            <div class="rep-text align-right">
                <strong><span class="nobr">325 250,00</span></strong>
            </div>
        </td>
    </tr></tbody></table><div style="margin:0 0 10px 0; line-height:125%; padding:0;">Всего наименований 3, на сумму:
    <br>Триста двадцать пять тысяч двести пятьдесят рублей, 00 копеек (без НДС)</div>
<div style="position:relative; margin:50px 0 0 0;">
    <!--
    <div id="stamp_p"><img id="stamp_img" src="/pics/30_stamp36488.png" style="width:174px; left:-100px; top:21px;"></div>
    <div id="sign1_p"><img id="sign1_img" src="/pics/30_sign192778.png" style="width:115px; left:-26px;"></div>
    <div id="sign2_p"><img id="sign2_img" src="/pics/30_sign213426.png" style="width:128px; left:-22px; top:13px;"></div>
    -->
    <div style="margin:10px 0 0 0; line-height:125%; width:200px; float:left; text-align:right;">
        Руководитель предприятия
    </div>
    <div style="margin:10px 0 0 0; line-height:125%; width:150px; float:left; text-align:left;">
        _________________________________________________
    </div>
    <div style="margin:10px 0 0 0; line-height:125%; width:200px; float:left; text-align:left;">
        Крылов А.Б.
    </div>

    <div class="clr"></div>

    <div style="margin:40px 0 0 0; line-height:125%; width:200px; float:left; text-align:right;">
        Главный бухгалтер
    </div>
    <div style="margin:40px 0 0 0; line-height:125%; width:150px; float:left; text-align:left;">
        _________________________________________________
    </div>
    <div style="margin:40px 0 0 0; line-height:125%; width:200px; float:left; text-align:left;">
        Летова Е.Е.
    </div>

    <div class="clr"></div>
</div>
</div>