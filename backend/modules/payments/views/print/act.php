<div class="print_div">

    <div style="font-size:17pt; color:555; line-height:125%; text-align:center; font-weight:bold;">
        АКТ № 25 от 04.02.2014
    </div>
    <div style="font-size:10pt; color:555; line-height:125%; text-align:center;">
        выполненных работ (оказанных услуг, переданных товаров) по Счету №  от
    </div>
    <div style="float:right; font-size:10pt; color:555; line-height:125%; text-align:right; margin:10px 0 10px 0;">
        г. Вельск
    </div>
    <div class="clr"></div>
    <div style="font-size:10pt; color:555; line-height:125%; text-align:left; margin:0 0 10px 0;">
        Общество с ограниченной ответственностью "Специальный строительные системы" (ИНН 1124577721), в дальнейшем "Исполнитель", и Общество с ограниченной ответственностью "Стройтехнологии" (ОГРН 1114217012700), в дальнейшем "Заказчик", совместно именуемые "Стороны", составили и подписали настоящий АКТ о том, что Исполнитель выполнил работы (оказал услуги, передал товары) в соответствии с условиями Счета № 15 от 01.02.2014, а именно:
    </div>
    <br>
    <table class="rep-table">
        <colgroup>
            <col style="width:25px;">
            <col>
            <col style="width:30px;">
            <col style="width:50px;">
            <col style="width:70px;">
            <col style="width:80px;">
            <col style="width:40px;">
            <col style="width:50px;">
            <col style="width:100px;">
        </colgroup>
        <thead>
        <tr>
            <th class="rep-cell rep-border">
                <div class="rep-text">№</div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Наименование товара (услуги)</div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Ед.<br>изм.</div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Коли-<br>чество<br>(объем)</div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Цена без <nobr>НДС</nobr></div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Сумма без <nobr>НДС</nobr></div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr"><nobr>НДС</nobr></div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Сумма <nobr>НДС</nobr></div>
            </th>
            <th class="rep-cell rep-border">
                <div class="rep-text wpbr">Всего с <nobr>НДС</nobr>, руб.</div>
            </th>
        </tr>
        </thead>
        <tbody><tr>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">1</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-left"><span>Промышленный станок СМП-1</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">шт</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">2</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">125 000,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">250 000,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">0%</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">0,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">250 000,00</span></div>
            </td>
        </tr>
        <tr>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">2</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-left"><span>Вибрационная установка МТВ КТи</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">шт</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">5</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">7 850,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">39 250,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">0%</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">0,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">39 250,00</span></div>
            </td>
        </tr>
        <tr>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">3</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-left"><span>Система пусковой установки СПУ-7</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">шт</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">8</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">4 500,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">36 000,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text"><span class="nobr">0%</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">0,00</span></div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right"><span class="nobr">36 000,00</span></div>
            </td>
        </tr>
        <tr>
            <td class="rep-cell" colspan="8">
                <div class="rep-text align-right">
                    <strong>Всего к оплате:</strong>
                </div>
            </td>
            <td class="rep-cell rep-border">
                <div class="rep-text align-right">
                    <strong><span class="nobr">325 250,00</span></strong>
                </div>
            </td>
        </tr></tbody></table><div style="margin:0 0 10px 0; line-height:125%; padding:0;">Всего наименований 3, на сумму:
        <br>Триста двадцать пять тысяч двести пятьдесят рублей, 00 копеек (без НДС)</div><div style="font-size:10pt; color:555; line-height:125%; text-align:center; margin:20px 0 0 0; font-weight:bold;">Подписи Сторон</div>
    <div style="position:relative; margin:20px 0 0 0;">
        <!--
        <div id="stamp_p"><img id="stamp_img" src="/pics/30_stamp36488.png" style="width:174px; left:-276px; top:102px;"></div>
        <div id="sign1_p<img" src="/pics/30_sign192778.png" style="width:142px; left:-195px; top:39px;"></div>
        -->
        <div style="float:left; width:320px; text-align:center;">
            <i>Исполнитель</i>
            <div style="font-size:10pt; color:555; line-height:125%; margin:10px 0 10px 0;">
                ООО Спецстрой
            </div>
            <div style="font-size:10pt; color:555; line-height:125%; margin:30px 50px 20px 50px; border:solid #333; border-width:1px 0 0 0;">
                (подпись)
            </div>
            М.П.
        </div>
        <div style="float:right; width:320px; text-align:center;">
            <i>Клиент</i>
            <div style="font-size:10pt; color:555; line-height:125%; margin:10px 0 10px 0;">
                ООО "Стройтехнологии"
            </div>
            <div style="font-size:10pt; color:555; line-height:125%; margin:30px 50px 20px 50px; border:solid #333; border-width:1px 0 0 0;">
                (подпись)
            </div>
            М.П.
        </div>

        <div class="clr"></div>

    </div>
</div>