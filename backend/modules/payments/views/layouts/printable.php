<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="resourse-type" content="document">
    <meta name="document-state" content="dynamic">
    <meta http-equiv="content-type" content="text/html">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <title>Печать / Счет-фактура № 145</title>
    <link rel="stylesheet" type="text/css" href="/css/print.css">

</head>
<body class="print_body" onload="window.print()">
<?=$content?>
</body>
</html>
