<?php

namespace app\modules\payments\models;

use Yii;
use app\modules\reserves\models\Reserve;
use app\modules\bands\models\Band;
/**
 * This is the model class for table "payments".
 *
 * @property string $id
 * @property string $reserve_id
 * @property string $band_id
 * @property double $value
 * @property string $date
 */
class Payment extends \yii\db\ActiveRecord
{
    public function __construct()
    {
        $this->user_id = Yii::$app->user->id;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reserve_id', 'band_id'], 'required'],
            [['reserve_id', 'band_id'], 'integer'],
            [['value'], 'number'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reserve_id' => 'Резерв',
            'band_id' => 'Клиент',
            'value' => 'Сумма оплаты',
            'date' => 'Дата',
            'user_id' => 'Принимающий оплату',
        ];
    }

    public function beforeSave($insert){
        $date = new \DateTime($this->date);
        $this->date = $date->format('Y-m-d H:i:s');
        return  parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes){
        $this->reserve->update();
        $this->band->update();
        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete(){
        $this->reserve->update();
        $this->band->update();
        parent::afterDelete();
    }
    public function getReserve(){
        return $this->hasOne('app\modules\reserves\models\Reserve', array('id' => 'reserve_id'));
    }
    public function getDiscount(){
        return $this->hasOne('app\modules\payments\models\Discount', array('id' => 'discount_id'));
    }
    public function getUser(){
        return $this->hasOne('backend\modules\users\models\User', array('id' => 'user_id'));
    }
    public function getBand(){
        return $this->hasOne('app\modules\bands\models\Band', array('id' => 'band_id'));
    }
}
