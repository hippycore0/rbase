<?php

namespace app\modules\payments\controllers;

use app\components\AjaxController;
use backend\components\HistoryHelper;
use Yii;
use app\modules\bands\models\Band;
use app\modules\payments\models\Payment;
use app\modules\payments\models\PaymentSearch;
use app\modules\reserves\models\Reserve;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
/**
 * DefaultController implements the CRUD actions for Payment model.
 */
class DefaultController extends AjaxController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('createPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('createPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new Payment();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            HistoryHelper::saveHistory([
                'id' => $model->id,
                'action' => Yii::$app->controller->action->id,
                'controller' => Yii::$app->controller->id,
                'module' =>  $model::className(),
                'user_id' => Yii::$app->user->id,
                'comment' => $model->value.' руб.'
            ]);
            Yii::$app->session->setFlash('payment_success','Оплата произведена');
            return $this->redirect(['/reserves/default/view', 'id' => $model->reserve_id]);
        } else {
            return $this->render('@backend/modules/reserves/views/default/payment', [
                'model' => Reserve::findOne($model->reserve_id),
                'payment' => $model
            ]);
        }
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('editPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            HistoryHelper::saveHistory([
                'id' => $model->id,
                'action' => Yii::$app->controller->action->id,
                'controller' => Yii::$app->controller->id,
                'module' =>  $model::className(),
                'user_id' => Yii::$app->user->id,
            ]);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if (!Yii::$app->user->can('editPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);
        HistoryHelper::saveHistory([
            'id' => $model->id,
            'action' => Yii::$app->controller->action->id,
            'controller' => Yii::$app->controller->id,
            'module' =>  $model::className(),
            'user_id' => Yii::$app->user->id,
        ]);
        $model->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
