<?php

namespace app\modules\payments\controllers;

class PrintController extends \yii\web\Controller
{
    var $layout = 'printable';
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/main';
        return $this->render('index');
    }

    /**
    Счет
     */
    public function actionBill(){
        return $this->render('bill');
    }

    /**
     Акт
     */
    public function actionAct(){
        return $this->render('act');
    }

    /**
    Товарный чек
     */
    public function actionCachememo(){
        return $this->render('cachememo');
    }

    /**
    Приходный ордер
     */
    public function actionInvoice(){
        return $this->render('invoice');
    }

    /**
    Договор
     */
    public function actionTerm(){
        return $this->render('term');
    }
}
