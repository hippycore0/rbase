<?php

namespace app\modules\bands\controllers;

use app\components\AjaxController;
use Yii;
use app\modules\bands\models\Band;
use app\modules\bands\models\BandSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\db\Query;
use yii\web\ForbiddenHttpException;
/**
 * DefaultController implements the CRUD actions for Band model.
 */
class DefaultController extends AjaxController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    /**
     * Lists all Band models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('createClient')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new BandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Band model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('createClient')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Band model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createClient')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new Band();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Band model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('edit')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Band model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('edit')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionJson($term = null, $id = null) {
        $out = ['more' => false];

        if (!is_null($term)) {
            $query = new Query;
            $query->select('id, title AS text')
                ->from('bands')
                ->where('title LIKE "%' . $term .'%"')
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Band::findOne($id)->title];
        }
        else {
            $out['results'] = ['id' => 0, 'text' => 'Совпадений не найдено'];
        }
        echo Json::encode($out);
    }

    /**
     * Finds the Band model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Band the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Band::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUri($offset=0){
        $models = Band::find()->limit(1000)->offset($offset)->all();
        foreach($models as $model){
            $model->save();
        }
        if($offset - Band::find()->count() < 1000){
            $offset = $offset+1000;
            $this->redirect('/bands/default/uri?offset='.$offset);
        }else{
            $this->redirect('/');
            Yii::$app->session->setFlash('success','Клиенты обновлены. '.$offset);
        }
    }
}
