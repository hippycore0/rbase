<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\bands\models\Band */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="band-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены что хотите удалить позицию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?
    $infoTab = [
        'label' => 'Информация',
        'content' =>  DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'summary_hours',
            'summary_price',
            'description',
            'phone',
            'contact',
            'email:email',
            'debt',
            //'type_id',
            //'director_id',
            //'status',
        ],
    ])
    ];
    $fineTab = [
        'label' => 'Штрафы',
        'content' => $this->render('@backend/modules/reserves/views/default/_fines_tab', ['models' => $model->getFined()]),
    ];
    $paymentsTab = [
        'label' => 'Оплаты',
        'content' => $this->render('@backend/modules/payments/views/_payments_tab', ['models' => $model->getPayments()])
    ];
    $tabItems = [$infoTab];
    if($model->getFined()->count() > 0):
        array_push($tabItems,$fineTab);
    endif;
    if($model->getPayments()->count() > 0):
        array_push($tabItems,$paymentsTab);
    endif;
    echo Tabs::widget([
        'items' => $tabItems,
    ]);
    ?>

</div>
