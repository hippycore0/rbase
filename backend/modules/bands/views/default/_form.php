<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\bands\models\Band */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="band-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'type_id')->dropDownList([0=>'Не задано',1=>'Клиент реп. базы',2=>'Клиент фотостудии'],['prompt'=>'Тип клиента']) ?>
    <h2>Контакты</h2>
    <?= $form->field($model, 'contact')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999-99-99',
        'options' => [
            'class' => 'form-control'
        ]
    ]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'director_id')->textInput() ?>
    <div class="row">
        <div class="col-xs-6"><?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success ' : 'btn btn-primary']) ?></div>
        <div class="col-xs-6"><?= Html::a('Отменить',['/'], ['class' =>  'btn btn-primary pull-right']) ?></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
