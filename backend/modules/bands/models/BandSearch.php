<?php

namespace app\modules\bands\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\bands\models\Band;

/**
 * BandSearch represents the model behind the search form about `app\modules\bands\models\Band`.
 */
class BandSearch extends Band
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'summary_hours', 'type_id', 'director_id', 'status'], 'integer'],
            [['title', 'description', 'phone', 'contact', 'email'], 'safe'],
            [['summary_price', 'debt'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Band::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'summary_hours' => $this->summary_hours,
            'summary_price' => $this->summary_price,
            'debt' => $this->debt,
            'type_id' => $this->type_id,
            'director_id' => $this->director_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
