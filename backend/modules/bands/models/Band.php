<?php

namespace app\modules\bands\models;

use app\modules\reserves\models\Reserve;
use app\modules\payments\models\Payment;
use Yii;

/**
 * This is the model class for table "bands".
 *
 * @property integer $id
 * @property string $title
 * @property integer $summary_hours
 * @property double $summary_price
 * @property string $description
 * @property string $phone
 * @property string $contact
 * @property string $email
 * @property double $debt
 * @property integer $type_id
 * @property integer $director_id
 * @property integer $status
 */
class Band extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'phone','type_id'], 'required'],
            [['summary_hours', 'type_id', 'director_id', 'status'], 'integer'],
            [['summary_price', 'debt'], 'number'],
            ['email', 'email'],
            [['title', 'description', 'contact','phone'], 'string', 'max' => 255]
        ];
    }

    public function getReserves(){
        return $this->hasMany(Reserve::className(),array('band_id' => 'id'));
    }

    public function getPayments(){
        return $this->hasMany(Payment::className(),array('band_id' => 'id'));
    }

    public function getTotalFine(){
        $fine = 0;
        foreach($this->getReserves()->where(['status'=>'fine'])->all() as $reserve){
            $fine+=$reserve->getFine();
        }
        return $fine;
    }

    public function getFined(){
        return $this->getReserves()->where(['status'=>'fine']);
    }

    public function getCanceled(){
        return $this->getReserves()->where(['status'=>'cancel']);
    }

    public function afterSave($insert, $changedAttributes){
        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind(){
        return parent::afterFind();
    }

    public function beforeSave($insert){
        $this->debt = $this->getTotalFine();
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'summary_hours' => 'Отыграно часов',
            'summary_price' => 'Потрачено денег',
            'description' => 'Описание',
            'phone' => 'Телефон',
            'contact' => 'Контактное лицо',
            'email' => 'Email',
            'debt' => 'Задолжность',
            'type_id' => 'Тип',
            'director_id' => 'Плательщик',
            'status' => 'Особая отметка',
        ];
    }

}
