<?php

namespace app\modules\bands;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\bands\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
