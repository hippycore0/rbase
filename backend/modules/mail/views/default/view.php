<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Email;

/* @var $this yii\web\View */
/* @var $model common\models\Email */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Адреса рассылки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Вы действительно хотите удалить адрес?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'group',
                'value' => isset(Email::getGroups()[$model->group]) ? Email::getGroups()[$model->group] : "",
            ],
            'email:email',
        ],
    ]) ?>

</div>
