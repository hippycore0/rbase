<?php

namespace backend\modules\mail;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\mail\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
