<?php

namespace app\modules\rooms\models;

use Yii;

/**
 * This is the model class for table "bases".
 *
 * @property integer $id
 * @property string $title
 * @property string $address
 * @property integer $group_id
 */
class Base extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bases';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'address'], 'string', 'max' => 255]
        ];
    }

    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['base_id' => 'id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'address' => 'Адрес',
        ];
    }
}
