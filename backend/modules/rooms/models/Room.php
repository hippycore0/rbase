<?php

namespace app\modules\rooms\models;

use Yii;
use app\modules\reserves\models\Reserve;

/**
 * This is the model class for table "rooms".
 *
 * @property integer $id
 * @property string $title
 * @property string $color
 * @property integer $base_id
 * @property integer $state
 * @property double $price
 * @property string $description
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','price'], 'required'],
            [['base_id', 'state'], 'integer'],
            [['price'], 'number'],
            [['title', 'color'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 255]
        ];
    }

    public function getReserves()
    {
        return $this->hasMany(Reserve::className(), ['room_id' => 'id']);
    }

    public function getBase()
    {
        return $this->hasOne(Base::className(), ['id' => 'base_id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'color' => 'Цвет',
            'base_id' => 'База',
            'state' => 'Активность',
            'price' => 'Цена',
            'description' => 'Описание',
        ];
    }

    public function getActiveReservesQuery($start,$end){
        return $this->getReserves()->joinWith('schedule', false,'INNER JOIN')->where('schedule.timestamp BETWEEN :start AND :end',[':start'=>$start->format('U'),':end'=>$end->format('U')])->andWhere('reserves.status != :status',[':status'=>'cancel']);
    }

    public function getSumHoursByPeriod($start,$end){
        $array = [];
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach ( $period as $dt ){
            $start = $dt;
            $end = clone $start;

            $array[] = $this->getActiveReservesQuery($start,$end->modify('+24 hours'))->sum('reserves.hours')==NULL?(int)0:(int)$this->getActiveReservesQuery($start,$end->modify('+24 hours'))->sum('reserves.hours');
        }
        return $array;
    }

    public function getFineByHours($hours=1){
        return $this->price*$hours/2;
    }

    public static function getForList(){
        $arr = \app\modules\rooms\models\Room::find()->orderBy('base_id')->asArray()->all();
        foreach($arr as $key => $room){
            $arr[$key]['title'].= ' :: '.Base::findOne($room['base_id'])->title.' - '.$room['price'].' руб./ч.';
        }
        return $arr;
    }
}
