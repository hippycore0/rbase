<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\ColorInput;
/* @var $this yii\web\View */
/* @var $model app\modules\rooms\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'base_id')->dropDownList(yii\helpers\ArrayHelper::map(\app\modules\rooms\models\Base::find()->all(), 'id', 'title'),['prompt'=>'Укажите базу где оказывается данная услуга','value'=>$model->base_id]) ?>

    <?= $form->field($model, 'state')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'color')->widget(ColorInput::className(),[

    ]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
