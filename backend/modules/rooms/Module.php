<?php

namespace app\modules\rooms;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\rooms\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
