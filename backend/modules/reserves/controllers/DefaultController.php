<?php

namespace app\modules\reserves\controllers;

use app\models\History;
use app\models\Schedule;
use app\modules\bands\models\Band;
use app\modules\rooms\models\Room;
use app\modules\services\models\Service;
use Yii;
use app\modules\reserves\models\Reserve;
use app\modules\reserves\models\ReserveSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\components\HistoryHelper;
use app\components\AjaxController;
/**
 * DefaultController implements the CRUD actions for Reserve model.
 */
class DefaultController extends AjaxController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        parent::beforeAction($action);
        \backend\models\UserSession::initSession();
        return true;
    }
    /**
     * Lists all Reserve models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewTable')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new ReserveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reserve model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('createReserve')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reserve model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
     * reserve => [timestamp =>['room'=>$model->room_id]]
     * */
    public function actionCreate()
    {

        if (!Yii::$app->user->can('createReserve')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = new Reserve();
        $params = Yii::$app->request->post();

        if ($model->load($params)) {

            if(empty($model->end)) {
                $model->end = $model->start;
            }
            $model->is_night = (bool)$params['Reserve']['is_night']?true:false;

            if($model->is_night){
                $model->start_hour = 0;
                $model->hours = 6;
            }
            $model->status = 'ready';
            $model->summary_payment = 0;

            if($model->save()){
                HistoryHelper::saveHistory([
                    'id' => $model->id,
                    'action' => Yii::$app->controller->action->id,
                    'controller' => Yii::$app->controller->id,
                    'module' =>  $model::className(),
                    'user_id' => Yii::$app->user->id
                ]);

                Yii::$app->session->set('schedule_start',\DateTime::createFromFormat('Y-m-d',$model->start)->format('d.m.Y'));
                $model->sendMail('Новый резерв','reserve_created');
                return $this->redirect('/');
            }else{
                /* Validation Error */
                return $this->render('create', [
                    'model' => $model
                ]);
            }

        }else{
            $params = Yii::$app->request->get();
            if (isset($params['room']))
                $model->room_id = $params['room'];

            if (isset($params['hour'])){
                $model->start_hour = $params['hour'];
                if($model->start_hour == 0){
                    $model->is_night = 1;
                    $model->hours = 6;
                }
            }

            if (isset($params['date']))
                $model->start = $params['date'];

            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionMail($id){
        $model = $this->findModel($id);
        $this->layout = '@backend/mail/layouts/html';
        return $this->render('@backend/mail/clients/reserve_cancel',['model'=>$model]);
    }

    public function actionCancel($id){
        /*Проверить сколько осталось до начала брони*/
        $model = $this->findModel($id);
        $reserve_time = \DateTime::createFromFormat('d.m.Y',$model->start);
        $current_time = new \DateTime();
        $current_time->modify('+1 day');
        $result = $reserve_time->diff($current_time);
        /*Если меньше 48 часов то начсляем штраф 50% от стоимости*/
        if((int)$result->format('%R%a') > -2){
            Yii::$app->session->setFlash('error','Резерв отменен менее чем за 48 часов. Штраф +50%: <strong>'.$model->calculateFine().'</strong> руб.');
            $model->status = 'fine';
            HistoryHelper::saveHistory([
                'id' => $model->id,
                'action' => Yii::$app->controller->action->id,
                'controller' => Yii::$app->controller->id,
                'module' =>  $model::className(),
                'user_id' => Yii::$app->user->id,
                'comment' => 'Штраф: '.$model->calculateFine()
            ]);
        }else{
            Yii::$app->session->setFlash('success','Резерв отменен');
            $model->status = 'cancel';
            HistoryHelper::saveHistory([
                'id' => $model->id,
                'action' => Yii::$app->controller->action->id,
                'controller' => Yii::$app->controller->id,
                'module' =>  $model::className(),
                'user_id' => Yii::$app->user->id,
                'comment' => 'Без штрафа'
            ]);
        }
        if($model->update()){
            $model->unlinkAll('services');
            $model->band->update();
            $model->sendMail('Резерв отменен','reserve_cancel');
            return $this->redirect('/');
        }else{
            Yii::$app->session->setFlash('error','Ошибка');
            return $this->redirect(['detail', 'id' => $model->id]);
        }
    }

    public function actionDefine($id){
        $model = $this->findModel($id);

        if($model->status == 'fine'){
            $model->status = 'cancel';
            $model->scenario = 'define';
            if($model->save()){
                HistoryHelper::saveHistory([
                    'id' => $model->id,
                    'action' => Yii::$app->controller->action->id,
                    'controller' => Yii::$app->controller->id,
                    'module' =>  $model::className(),
                    'user_id' => Yii::$app->user->id,
                ]);
                $model->band->save();
                Yii::$app->session->setFlash('success','Штраф отменен');
            }
        }
        return $this->redirect('/');
    }

    /**
     * Updates an existing Reserve model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('underCall')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);
        $params = Yii::$app->request->post();

        if($model->load($params)){
            if(empty($model->end)):
                $model->end = $model->start;
            endif;
            if($model->is_night){
                $model->start_hour = 0;
                $model->hours = 6;
            }

            if ($model->save()) {
                HistoryHelper::saveHistory([
                    'id' => $model->id,
                    'action' => Yii::$app->controller->action->id,
                    'controller' => Yii::$app->controller->id,
                    'module' =>  $model::className(),
                    'user_id' => Yii::$app->user->id,
                ]);
                Yii::$app->session->setFlash('success','Информация обновлена');
                $model->sendMail('Резерв изменен','reserve_updated');
                return $this->redirect('/');
            }else{
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionPayment($id){
        if (!Yii::$app->user->can('createPayment')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('payment', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDetail($id){
        if (!Yii::$app->user->can('createReserve')) {
            throw new ForbiddenHttpException('Access denied');
        }
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionFlexdiscount($id){
        if (!Yii::$app->user->can('underCall')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);
        $params = Yii::$app->request->post();
        if($model->load($params) && $model->validate()){
            Yii::$app->session->setFlash('success_flex','Скидка обновлена');
            $model->update();
        }else{
            Yii::$app->session->setFlash('error_flex','Ошибка');
        }
        return $this->redirect('/reserves/default/payment?id='.$id);
    }

    /**
     * Deletes an existing Reserve model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('edit')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);
        Schedule::deleteAll('reserve_id = :id', [':id' => $model->id]);
        HistoryHelper::saveHistory([
            'id' => $model->id,
            'action' => Yii::$app->controller->action->id,
            'controller' => Yii::$app->controller->id,
            'module' =>  $model::className(),
            'user_id' => Yii::$app->user->id,
        ]);

        $model->delete();
        return $this->redirect('/');
    }

    public function actionRemove($id)
    {
        if (!Yii::$app->user->can('underCall')) {
            throw new ForbiddenHttpException('Access denied');
        }
        /*Проверить сколько осталось до начала брони*/
        $model = $this->findModel($id);

        $model->status = 'remove';
        $model->scenario = 'remove';
        if($model->update()){
            HistoryHelper::saveHistory([
                'id' => $model->id,
                'action' => Yii::$app->controller->action->id,
                'controller' => Yii::$app->controller->id,
                'module' =>  $model::className(),
                'user_id' => Yii::$app->user->id,
                'comment' => ''
            ]);
            return $this->redirect('/');
        }else{
            Yii::$app->session->setFlash('error','Ошибка');
            return $this->redirect(['detail', 'id' => $model->id]);
        }
    }

    /**
     * Finds the Reserve model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reserve the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reserve::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDay($id){
        /*Найти день в рпасписаниии*/
        $schedule = Schedule::findOne($id);

        /*Определить какие часы входят в этот день*/
        $dayStart = strtotime(date('d.m.Y',$schedule->timestamp));
        $dayEnd = $dayStart+24*60*60;

        HistoryHelper::saveHistory([
            'id' => $schedule->reserve_id,
            'action' => Yii::$app->controller->action->id,
            'controller' => Yii::$app->controller->id,
            'module' =>  Reserve::className(),
            'user_id' => Yii::$app->user->id,
            'comment' => date('d.m.Y',$schedule->timestamp)
        ]);

        /*Удалить часы из расписания*/
        Schedule::deleteAll('timestamp >= :dayStart AND timestamp < :dayEnd AND reserve_id = :reserveID',[':dayStart'=>$dayStart,':dayEnd'=>$dayEnd,':reserveID'=>$schedule->reserve_id]);

        return $this->redirect('/');
    }

    public function actionDaycancel($id){
        /*Найти день в рпасписаниии*/
        $schedule = Schedule::findOne($id);
        /*Посчитать штраф*/
        $brothers = $schedule->getBrothers();
        $fine = $schedule->room->getFineByHours($brothers->count());

        /*Иницилизировать новый резерв с такими же параметрами, но только на текущий день*/
        var_dump($schedule->reserve->attributes);
        $newReserve = new Reserve();
        $newReserve->load($schedule->reserve->attributes);
        $newReserve->hours = $brothers->count();
        $newReserve->start = date('Y-m-d',$schedule->timestamp);
        if($newReserve->validate()){
            echo 'valid';
        }else{
            var_dump($newReserve->getErrors());
        }
        /*удалить часы*/

        /*сохранить новый резерв*/
        echo $fine;
        /*Добавить штраф всему резерву*/

    }

    public function actionUri($offset=0){
        $reserves = Reserve::find()->limit(1000)->offset($offset)->all();
        foreach($reserves as $reserve){
            if($reserve->summary_price != $reserve->getTotalWithDiscount(true) || $reserve->summary_payment != (int)$reserve->getSummaryPayment()){
                $reserve->update();
            }
        }
        if($offset - Reserve::find()->count() < 1000){
            $offset = $offset+1000;
            $this->redirect('/reserves/default/uri?offset='.$offset);
        }else{
            $this->redirect('/');
            Yii::$app->session->setFlash('success','Резервы обновлены');
        }
    }
}
