<?php

namespace app\modules\reserves\models;


use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\web\UrlManager;
use yii\helpers\Html;
use common\components\Days;
use app\modules\bands\models\Band;
use app\modules\payments\models\Discount;
use app\modules\services\models\Service;
use app\modules\rooms\models\Base;
use app\modules\payments\models\Payment;
use app\models\Schedule;
use app\modules\reserves\models\ReservePacks;

/**
 * This is the model class for table "reserves".
 *
 * @property integer $id
 * @property integer $room_id
 * @property integer $band_id
 * @property double $summary_price
 * @property double $summary_payment
 * @property string $start
 * @property string $end
 * @property integer $hours
 * @property integer $start_hour
 */
class Reserve extends \yii\db\ActiveRecord implements Linkable
{
    public $new_services = [];
    public $timestamps;
    public $fine;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserves';
    }

    public function scenarios(){
        return [
            'define'=>['status'],
            'remove'=>['status'],
            'default'=>['room_id','band_id','is_night','hours','start_hour','start', 'end','new_services','discount_id','summary_price', 'summary_payment','status','discount_flexible','user_create','created_at','updated_at']
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'band_id','hours', 'start_hour','start'], 'required'],
            [['room_id', 'band_id','discount_id', 'hours','user_create','created_at','updated_at','reserve_pack_id'], 'integer'],
            [['summary_price', 'summary_payment','start_hour','hours'], 'number'],
            [['start', 'end'], 'valid_date'],
            [['start_hour'],'check_call_start_hour'],
            [['hours'],'check_call_min_hours'],
            [['start','start_hour', 'end'],'check_reserve_time'],
            [['new_services'],'check_services'],
            [['is_night'],'boolean'],
            [['status'],'string', 'max' => 100],
            [['room_id','start_hour'],'check_room_permission'],
            [['discount_flexible'],'number','max'=>$this->getRoomCost(false)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'room_id' => 'Комната/Услуга',
            'roomTitle' => 'Комната/Услуга',
            'band_id' => 'Клиент',
            'bandTitle' => 'Клиент',
            'summary_price' => 'Полная стоимость',
            'summary_payment' => 'Оплачено',
            'start' => 'Дата брони',
            'end' => 'Постоянка до',
            'hours' => 'Количество часов брони',
            'start_hour' => 'Время начала брони',
            'discount_id' => 'Скидка',
            'new_services'=> 'Доп. услуги',
            'is_night'=> 'Ночь',
            'fine' => 'Штраф',
            'discount_flexible'=>'Гибкая скидка',
            'reserve_pack_id'=>'Постоянка'
        ];
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['reserve/view', 'id' => $this->id], true),
        ];
    }


    public function beforeSave($insert){
        $this->start = date_create($this->start)->format('Y-m-d');
        $this->end=date_create($this->end)->format('Y-m-d');

        if($insert){
            $this->created_at = time('U');
            $this->user_create = Yii::$app->user->id;
        }else{
            $this->updated_at = time('U');
        }
        $this->summary_price = $this->getTotalWithDiscount(true);
        $this->summary_payment = (int)$this->getSummaryPayment();
        return  parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes){
        $this->unlinkAll('services', true);
        if(is_array($this->new_services)) {
            foreach ($this->new_services as $s_id) {
                $service = Service::findOne($s_id);
                if ($service != null) {
                    $this->link('services', $service);
                }
            }
        }
        $this->setTimestamps();
        Schedule::deleteAll('reserve_id = :id', [':id' => $this->id]);
        foreach($this->timestamps as $timestamp){
            $schedule = new Schedule();
            $schedule->room_id = $this->room_id;
            $schedule->band_id = $this->band_id;
            $schedule->timestamp = $timestamp;
            $schedule->reserve_id = $this->id;
            $schedule->save();
        }

       return parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind(){
        $from=\DateTime::createFromFormat('Y-m-d',$this->start);
        $this->start=$from->format('d.m.Y');
        $to=\DateTime::createFromFormat('Y-m-d',$this->end);
        $this->end=$to->format('d.m.Y');
        $this->new_services = array_keys($this->getServices()->asArray()->indexBy('id')->all());
        $this->fine = $this->getFine();
        if($this->summary_price <= 0){
            $this->summary_price = $this->getTotalWithDiscount(true);
        }
        return parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->unlinkAll('services', true);
            Payment::deleteAll('reserve_id = :reserve_id',[':reserve_id'=>$this->id]);
            Schedule::deleteAll('reserve_id = :id', [':id' => $this->id]);
            return true;
        }

        return false;
    }

    public function  beforeValidate(){
        $this->setTimestamps();
        $this->setData();
        return parent::beforeValidate();
    }

    public function getUser(){
        return $this->hasOne('app\modules\users\models\User', array('id' => 'user_create'));
    }
    public function getSummaryPayment(){
        return $this->getPayments()->sum('value');
    }
    public function getRoom(){
        return $this->hasOne('app\modules\rooms\models\Room', array('id' => 'room_id'));
    }
    public function getBand(){
        return $this->hasOne('app\modules\bands\models\Band', array('id' => 'band_id'));
    }
    public function getBandTitle(){
        return $this->band->title;
    }
    public function getBandRoom(){
        return $this->room->title;
    }
    public function getDiscount(){
        return $this->hasOne('app\modules\payments\models\Discount', array('id' => 'discount_id'));
    }
    public function getServices(){
        return $this->hasMany(Service::className(),array('id' => 'service_id'))
            ->viaTable('reserved_services',['reserve_id'=>'id']);
    }
    public function getSchedule(){
        return $this->hasMany(Schedule::className(),['reserve_id' => 'id']);
    }
    public function getReservePack(){
        return $this->hasOne(ReservePacks::className(),array('id' => 'reserve_pack_id'));
    }
    public function getBase(){
        return Base::findOne($this->room->base_id);
    }

    public function getHistory(){
        return $this->hasMany('app\models\History', array('item_id' => 'id'))->orderBy(['create_at'=>SORT_DESC])->where(['module'=>$this::className()]);
    }
    public function getPayments(){
        return $this->hasMany('app\modules\payments\models\Payment', array('reserve_id' => 'id'));
    }
    public function getServicesCost(){
        $summary = 0;
        if(count($this->services) > 0){
            foreach($this->services as $service):
                $summary += $service->price*$this->hours;
            endforeach;
        }
        return $summary;
    }

    public function getRoomCost($discount = false){
        if(isset($this->id)){
            $roomSummary = $this->room->price*($this->hours*$this->getWeeks());
            if($this->is_night){
                $roomSummary -= 200;
            }
            if($discount === true){
                $roomSummary -= $this->getDiscountCost();
            }
            return $roomSummary;
        }else{
            return 0;
        }

    }

    public function getDiscountCost(){
        if($this->discount_flexible){
            return $this->discount_flexible;
        }elseif($this->discount_id > 0){
            $discount_cost = 0;
            $discount = Discount::findOne($this->discount_id);
            if($discount->type == 1){
                $discount_cost = $discount->value;
            }elseif($discount->type == 2){
                $discount_cost = ($this->summary_price/100)*$discount->value;
            }
            return $discount_cost;
        }else{
            return 0;
        }
    }


    public  static  function getActiveReserves($start,$end,$band_id=false){
        $query = Reserve::getActiveReservesQuery($start,$end,$band_id);
        $schedules = $query->asArray()->all();
        $r = [];
        foreach($schedules as $schedule){
            $r[$schedule['timestamp']][$schedule['room_id']]['band_id'] = $schedule['band_id'];
            $r[$schedule['timestamp']][$schedule['room_id']]['reserve_id'] = $schedule['reserve_id'];
        }
        return $r;
    }

    public static function  getActiveReservesQuery($start,$end,$band_id=false){
        $query = Schedule::find()->where('timestamp BETWEEN :start AND :end',[':start'=>$start->format('U'),':end'=>$end->format('U')])->joinWith('reserve', false,'INNER JOIN')->andWhere(['reserves.status'=>'ready']);
        if($band_id > 0){
            $query = $query->andWhere(['schedule.band_id'=>$band_id]);
        }

        return $query;
    }
    public function getWeeks(){
        $datetime1 = new \DateTime($this->start);
        $datetime2 = new \DateTime($this->end);
        $interval = date_diff($datetime1,$datetime2);
        $i = ceil(((int)$interval->days+1)/7);
        return $i;
    }



    public function getFine(){
        if($this->status == 'fine'){
            return $this->calculateFine();
        }else{
            return 0;
        }
    }

    public function  getTotalWithDiscount($discount = true,$round = true){
        $summary = $this->getRoomCost($discount) + $this->getServicesCost() + $this->getFine();
        if($round == true){
            return round($summary/10)*10;
        }else{
            return $summary;
        }
    }


    public function calculateFine(){
        return round(($this->getRoomCost(false)/2)/10)*10;
    }

    public function needPay(){
        return $this->summary_price-$this->summary_payment;
    }

    public function getUrl(){
        return '/reserves/default/detail?id='.$this->id;
    }

    public function check_call_start_hour($attribute, $param){
        if($this->is_night == 0 && ($this->start_hour < 9 || $this->start_hour > 23) || ($this->start_hour + $this->hours > 24)){
            $this->addError($attribute, 'Услуги могут быть зарезервированы с 9:00 до 24:00, либо на всю ночь с 0:00 до 6:00');
        }elseif(!Yii::$app->user->can('underCall') && !Days::isWeekend($this->start) && $this->start_hour < 12 && $this->is_night == 0 && !($this->room_id == 10 || $this->room_id == 11)){
            $this->addError($attribute, 'В будние дни, время можно бронировать с 12:00');
        }elseif(!Yii::$app->user->can('underCall') && $this->is_night != 0){
            $this->addError('is_night', 'Недостаточно полномочий для резерва ночных репетиций, обратитесь к ночному администратору');
        }
        return true;
    }

    public function check_call_min_hours($attribute, $param){
        if(!Yii::$app->user->can('underCall') && !($this->room_id == 10 || $this->room_id == 12)){
            if($this->hours >=2){
                //если свторого часа то может забить либо 4 либо 5 часов (1 и 2 по модулю)
                //если с первого часа то может забить либо 3 либо 5  (0 и 2 по модулю)
                if(($this->start_hour%3 == 1 && $this->hours%3 == 0) || ($this->start_hour%3 == 0 && $this->hours%3 == 1) || $this->start_hour%3 == 2){
                    $this->addError($attribute, 'Время можно бронировать в пределах и кратно 3-х часовым сэтам: 9:00-12:00; 12:00-15:00; 15:00-18:00; 18:00-21:00; 21:00-24:00, 2 часа за сет минимум');
                }
            }else{
                $this->addError($attribute, 'Можно забронировать минимум 2 часа репетиции за сет');
            }
        }
        return true;
    }

    public function check_room_permission($attribute, $param){
        if(!Yii::$app->user->can('underCall') && $this->room_id == 12){
            $this->addError('room_id', 'Бронирование данной услуги осуществляется руководством');
        }
        if(!Yii::$app->user->can('underCall') && ($this->room_id == 10 || $this->room_id == 11) && $this->start_hour < 12){
            $this->addError($attribute,'Бронирование данной услуги осуществляется c 12:00');
        }
        return true;
    }

    public function check_services($attribute, $param){
        if(is_array($this->new_services)) {
            foreach ($this->new_services as $service_id) {
                $service = Service::findOne($service_id);
                if ($service->isAvailable($this) == false) {
                    $this->addError('new_services', 'Эта услуга занята: ' . $service->title);
                }
                if ($this->getBase()->id != $service->base_id) {
                    $this->addError('new_services', 'Ошибка. Услуга "' . $service->title . '" доступна только на базе "' . $service->getBase()->one()->title . '"');
                }
            }
        }
        return true;
    }

    function check_reserve_time($attribute, $param) {
        $data = json_decode($this->data);
        foreach($data as $timestamp => $i){
            $check = Schedule::find()->where('timestamp = :timestamp',[':timestamp'=>$timestamp])
                ->andWhere('schedule.band_id != :band',[':band'=>$this->band_id]) /*сделать проверку по номеру резерва а не номеру клиента*/
                ->andWhere(['reserves.status'=>'ready','schedule.room_id'=>$this->room_id])
            ->joinWith(['reserve']);
            if($check->count()>0){
                if(Yii::$app->user->can('call')){
                    $text = 'Время занято: '.date('d.m.Y H:i:s',$timestamp).', резерв №'.$check->one()->reserve_id;
                } else {
                    $text = 'Время занято';
                }
                $this->addError($attribute,$text);
                break;
            }
        }
    }

    function valid_date($attribute, $param){
        if(!$this->start_hour){
            $start_hour = 0;
        }else{
            $start_hour = $this->start_hour;
        }
        $start_time = new \DateTime($this->start.' '.$start_hour.':00');
        $now = new \DateTime('now');
        if($start_time < $now&&!Yii::$app->user->can('call')){
            $this->addError('start', 'Вы указали прошедшую дату: '.$start_time->format('d.m.Y H:i'));
        }
    }
    /**
     * Make timestamps array
     * @param date
     * @return array
     */
    public  function setTimestamps(){
        /*TODO: не верно считает колличество недель*/
        $weeks = $this->getWeeks();
        $data = [];
        for($i=0; $i<$weeks; $i++){
            $start_time = new \DateTime($this->start);
            $date = $start_time->modify('+'.$i.' weeks');
            for ($h = 0; $h < $this->hours; $h++) {
                $data[] = strtotime($date->format('d.m.Y') . ' ' . ($this->start_hour+$h) . ':00');
            }
        }
        $this->timestamps = $data;
    }

    public function getTimestamps(){
        /*Везде переделать на getSchedule, отказаться от reserve->date*/

        if(!is_array($this->timestamps)){
            $this->setTimestamps();
        }
        return $this->timestamps;
    }

    public function setData(){
        $data = [];
        foreach($this->timestamps as $timestamp){
            $data[$timestamp][$this->room_id] = $this->band_id;
        }
        $this->data = json_encode($data);
    }

    public function isActual(){
        $end =  \DateTime::createFromFormat('d.m.Y',$this->end);
        if($end->modify('+24 hours')->format('U') - date('U') > 0 && $this->status != 'fine'){
            return true;
        }else{
            return false;
        }
    }

    public function isDayDay(){
        return (int)$this->created_at - (int)date('U',strtotime($this->start)) > 0;
    }

    public function sendMail($subject,$template){
        if(!empty($this->band->email)){
            Yii::$app->mailer->compose('clients/'.$template,['model'=>$this])
                ->setFrom([Yii::$app->params['reserveFromMail']=>Yii::$app->params['companyName']])
                ->setTo($this->band->email)
                ->setSubject($subject)
                ->send();
        }
        Yii::$app->mailer->compose('admins/'.$template,['model'=>$this])
            ->setFrom([Yii::$app->params['reserveFromMail']=>Yii::$app->params['companyName']])
            ->setTo([Yii::$app->params['reserveAdminMail']])
            ->setSubject($subject)
            ->send();
    }
}
