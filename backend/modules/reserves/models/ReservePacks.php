<?php

namespace app\modules\reserves\models;

use Yii;

/**
 * This is the model class for table "reserve_packs".
 *
 * @property integer $id
 * @property integer $band_id
 */
class ReservePacks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserve_packs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['band_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getReserves(){
        return $this->hasMany(Reserve::className(),['reserve_pack_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'band_id' => 'Band ID',
        ];
    }
}
