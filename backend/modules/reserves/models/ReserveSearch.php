<?php

namespace app\modules\reserves\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\reserves\models\Reserve;

/**
 * ReserveSearch represents the model behind the search form about `app\modules\reserves\models\Reserve`.
 */
class ReserveSearch extends Reserve
{
    public $bandTitle;
    public $roomTitle;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'room_id', 'band_id', 'hours', 'start_hour'], 'integer'],
            [['summary_price', 'summary_payment'], 'number'],
            [['bandTitle','roomTitle'],'string'],
            [['start', 'end','bandTitle','roomTitle'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['band.title']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reserve::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $query->joinWith(['band' => function($query) { $query->from(['band' => 'bands']); }]);
        $query->joinWith(['room' => function($query) { $query->from(['room' => 'rooms']); }]);

        // enable sorting for the related column

        $dataProvider->sort->attributes['bandTitle'] = [
            'asc' => ['bands.title' => SORT_ASC],
            'desc' => ['bands.title' => SORT_DESC],
            'label' => 'Клиент'
        ];

        $dataProvider->sort->attributes['roomTitle'] = [
            'asc' => ['rooms.title' => SORT_ASC],
            'desc' => ['rooms.title' => SORT_DESC],
            'label' => 'Клиент'
        ];

        if (!($this->load($params) && $this->validate())) {
            $query->joinWith(['band']);
            $query->joinWith(['room']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reserves.id' => $this->id,
            'room_id' => $this->room_id,
            'band_id' => $this->band_id,
            'summary_price' => $this->summary_price,
            'summary_payment' => $this->summary_payment,
            'start' => $this->start,
            'end' => $this->end,
            'hours' => $this->hours,
            'start_hour' => $this->start_hour,
        ]);
        $query->joinWith(['band' => function ($q) {
            $q->where('bands.title LIKE "%' . $this->bandTitle . '%"');
        }]);
        $query->joinWith(['room' => function ($q) {
            $q->where('rooms.title LIKE "%' . $this->roomTitle . '%"');
        }]);
        return $dataProvider;
    }
}
