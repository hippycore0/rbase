<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use app\modules\rooms\models\Room;
use app\modules\bands\models\Band;
/* @var $this yii\web\View */
/* @var $model app\modules\reserves\models\Reserve */
/* @var $form yii\widgets\ActiveForm */
$url = \yii\helpers\Url::to(['/bands/default/json']);
$this->title = 'Оплата резерва';
$initScript = <<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
?>
<h1>Оплата</h1>
<?if(Yii::$app->session->hasFlash('success_flex')):?>
    <?echo \yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-success',
        ],
        'body' => Yii::$app->getSession()->getFlash('success_flex'),
    ]);?>
<?endif;?>
<?if(Yii::$app->session->hasFlash('error_flex')):?>
    <?echo \yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-danger',
        ],
        'body' => Yii::$app->getSession()->getFlash('error_flex'),
    ]);?>
<?endif;?>
<a href="#" id="flexible_discount_link">Задать гибкую скидку</a>
<Br /><Br />
<?php $form = ActiveForm::begin([
    'action' => '/reserves/default/flexdiscount?id='.$model->id
]); ?>
<div id="flexible_discount" style="display: none" class="well">
    <div class="form-group">
    <?=$form->field($model,'discount_flexible')?>
    </div>
    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    <br />
</div>
<?php ActiveForm::end(); ?>
<div class="reserve-form">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'validateOnChange' => false,
        'action' => '/payments/default/create'
    ]); ?>
    <?=Html::hiddenInput('Payment[reserve_id]',$model->id)?>
    <div class="form-group">
        <?=Html::label($model->getAttributeLabel('room_id').':')?> <?= Room::find()
            ->where(['id' => $model->room_id])
            ->one()->title?>
        <?=Html::hiddenInput('Payment[room_id]',$model->room_id)?>
    </div>
    <div class="form-group">
        <?=Html::label($model->getAttributeLabel('band_id').':')?> <?= Band::find()
            ->where(['id' => $model->band_id])
            ->one()->title?>
        <?=Html::hiddenInput('Payment[band_id]',$model->band_id)?>
    </div>
    <div class="form-group">
        <?=Html::label('Дата:')?>
        <?=date('d.m.Y H:i')?>
        <?=Html::hiddenInput('Payment[date]',date('d.m.Y H:i'))?>
    </div>
    <?if($model->getServices()->all()):?>
        <div class="form-group">
            <?=Html::label('Включает '.$model->getServices()->count().' доп. услуг(и)')?>
        </div>
    <?endif?>
    <div class="form-group">
        <?if($model->getDiscountCost() > 0): ?>
            <?=Html::label($model->getAttributeLabel('discount_id').':')?>
            <?if($model->discount_flexible > 0):?>
                <?=$model->discount_flexible?> руб.
            <?else:?>
                <?= $model->discount->title?>
                <?=Html::hiddenInput('discount_id',$model->discount_id)?>
            <?endif;?>
        <?endif;?>
    </div>
    <div class="form-group">
        <?=Html::label($model->getAttributeLabel('summary_price').":")?>
        <?= $model->summary_price; ?> руб.
        <?=Html::hiddenInput('summary_price',$model->summary_price)?>
    </div>
    <div class="form-group">
        <?=Html::label($model->getAttributeLabel('summary_payment').':')?>
        <?= $model->summary_payment; ?> руб.
        <?=Html::hiddenInput('summary_payment',$model->summary_payment)?>
    </div>
    <div class="form-group">
        <?=Html::label('Сумма оплаты:')?>
        <?= Html::textInput('Payment[value]',$model->summary_price-$model->summary_payment,['class'=>'form-control']);?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>