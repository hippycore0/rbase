<?
use yii\helpers\Html;
?>
<div class="btn-group btn-group-<?=$class?>">
<?=$model->needPay() > 0&&$model->status!='remove'?Html::a('Оплатить', ['payment', 'id' => $model->id], ['class' => 'btn btn-success']):''?>
<?=$model->isActual()&&$model->status!='remove'?Html::a('Отменить бронь', ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning', 'data'=> ['confirm'=>' Резерв будет отменен менее чем за 48 часов. Штраф +50%: '.$model->calculateFine().' руб. Действительно отменить?']]):'';?>
<?=$model->status == 'fine'?Html::a('Отменить штраф', ['define', 'id' => $model->id], ['class' => 'btn btn-warning']):'';?>
<?=$model->status!='remove'?Html::a('Редактировать бронь', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']):'';?>
<?=$model->status!='remove'?Html::a('Удалить бронь', ['remove', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
    'confirm' => 'Уверены что хотите удалить позицию?',
    'method' => 'post',
    ],
    ]):'';?>
</div>