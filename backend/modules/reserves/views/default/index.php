<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reserves\models\ReserveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Резервы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserve-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый резерв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'label'=>'№',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->id;
                },
                'options' => [
                    'width'=>'5%'
                ]
            ],
            [
                'label'=>'Последнее действие',
                'format' => 'html',
                'value'=>function($model){
                    return $model->getHistory()->one()->action;
                }
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Дата/Время действия',
                'value'=> function($model){
                    return $model->getHistory()->one()->create_at;
                }
            ],
            [
                'attribute' => 'roomTitle',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->room->title, ['/rooms/default/view', 'id' => $model->room->id]);
                }
            ],
            [
                'attribute' => 'bandTitle',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->band->title, ['/bands/default/view', 'id' => $model->band->id]);
                }
            ],
            [
                'attribute' => 'start',
                'label' => 'Начало',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->start.' '.$model->start_hour.':00';
                }
            ],
            [
                'attribute' => 'user_create',
                'label' => 'Регистратор',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(\backend\modules\users\models\User::findOne($model->user_create)->username,['/users/manage/view','id'=>$model->user_create]);
                }
            ],
            // 'start',
            // 'end',
            // 'hours',
            // 'start_hour',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
