<?
/**
 * @var $models app\modules\payments\models\Payment
 */
use yii\data\ActiveDataProvider;
use app\modules\services\models\ServiceSearch;
use yii\grid\GridView;
use yii\helpers\Html;
$searchModel = new ServiceSearch();
$dataProvider = new ActiveDataProvider([
    'query' => $models
]);
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => '№',
            'value' => function ($model) {
                return $model->id;
            }
        ],
        'title',
        'price',
        [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{view}',
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = '/services/default/view?id=' . $model->id;
                    return $url;
                }
            }
        ],
    ],
]); ?>