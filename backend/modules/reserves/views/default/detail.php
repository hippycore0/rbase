<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\History;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\reserves\models\Reserve */
$this->registerJsFile('js/reserveView.js',['yii\web\JqueryAsset']);
$this->title = 'Резерв №'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Резервы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserve-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?if(Yii::$app->session->hasFlash('payment_success')):?>
        <?echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-success',
            ],
            'body' => Yii::$app->getSession()->getFlash('payment_success'),
        ]);?>
    <?endif;?>
    <?=$this->render('_controls',['model'=>$model,'class'=>'md'])?>
    <br /><br />
    <?
    $infoTab = [
        'label' => 'Информация',
        'content' => DetailView::widget([
            'model' => $model,
            'attributes' => [

                'id',
                [
                    'label' => 'Комната',
                    'attribute' => 'room_id',
                    'format' => 'html',
                    'value' => Html::a($model->room->title,['/rooms/default/view', 'id' => $model->room->id])
                ],
                [
                    'label' => 'Клиент',
                    'attribute' => 'band_id',
                    'format' => 'html',
                    'value' => Html::a($model->band->title, ['/bands/default/view', 'id' => $model->band->id])
                ],

                [
                    'attribute'=>'summary_price',
                    'format' => 'html',
                    'value' => $model->summary_price. ' руб.'
                ],
                [
                    'attribute'=>'Доп. услуги',
                    'format' => 'html',
                    'value' => $model->getServicesCost().' руб.'
                ],
                [
                    'attribute'=>'summary_payment',
                    'format' => 'html',
                    'value' => $model->summary_payment. ' руб.'
                ],
                'start',
                'end',
                [
                    'label'=>'С',
                    'format' => 'html',
                    'value' => $model->start_hour
                ],
                [
                    'label'=>'До',
                    'format' => 'html',
                    'value' => $model->start_hour + $model->hours
                ],
                'hours',
                'discount_id',
                'fine'
            ],
        ]),
        'active' => true
    ];
    $fineTab = [
        'label' => 'Штрафы',
        'content' => $this->render('_fines_tab', ['models' => $model->band->getFined()]),
    ];
    $scheduleTab = [
        'label' => 'Постоянка',
        'content' => $this->render('_schedule_tab', ['model' => $model])
    ];
    $paymentsTab = [
        'label' => 'Оплаты',
        'content' => $this->render('@backend/modules/payments/views/_payments_tab', ['models' => $model->getPayments()])
    ];
    $servicesTab = [
        'label' => 'Доп. услуги',
        'content' => $this->render('@backend/modules/services/views/_services_tab', ['models' => $model->getServices()])
    ];
    $historyTab = [
        'label' => 'История',
        'content' => $this->render('@backend/views/history/_widget',['model'=>History::find()->where(['like','module','Reserve'])->andWhere(['item_id'=>$model->id])->orderBy(['create_at'=>SORT_ASC])->all()])
    ];
    $tabItems = [$infoTab];

    if($model->band->getFined()->count() > 0):
        array_push($tabItems,$fineTab);
    endif;
    if($model->start != $model->end):
        array_push($tabItems,$scheduleTab);
    endif;
    if($model->getServices()->count() > 0):
        array_push($tabItems,$servicesTab);
    endif;
    if($model->getPayments()->count() > 0):
        array_push($tabItems,$paymentsTab);
    endif;
    array_push($tabItems,$historyTab);
    echo Tabs::widget([
        'items' => $tabItems,
    ]);
    ?>

</div>
