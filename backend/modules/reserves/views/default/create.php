<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\reserves\models\Reserve */

$this->title = 'Создать резерв';
$this->params['breadcrumbs'][] = ['label' => 'Резервы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserve-create">
    <? if (!Yii::$app->request->isAjax): ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?endif;?>
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
