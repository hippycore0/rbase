<?
use yii\helpers\Html;
if($model->start != $model->end):
    $reserved = [];
    ?>
    <h2>Постоянка</h2>

    <?foreach($model->getSchedule()->all() as $key=>$schedule):?>
    <?$reserved[$schedule->id] = date('d.m',$schedule->timestamp)?>
<?endforeach;?>
    <?$days = array_unique($reserved);?>

    <?foreach($days as $schedule_id => $day):?>
    <div class="row">
        <div class="col-xs-1">
            <h4><?=$day?></h4>
        </div>
        <div class="col-xs-4">
            <?=Html::a('Отменить',['/reserves/default/day/?id='.$schedule_id], ['class'=>'btn btn-default'])?>
        </div>
    </div>
<?endforeach;?>
<?endif;?>