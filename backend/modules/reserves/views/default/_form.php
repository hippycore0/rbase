<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\TouchSpin;
use kartik\widgets\DatePicker;
use dosamigos\multiselect\MultiSelect;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\reserves\models\Reserve */
/* @var $form yii\widgets\ActiveForm */
\backend\assets\ReserveAsset::register($this);
$url = \yii\helpers\Url::to(['/bands/default/json']);
$urlService = \yii\helpers\Url::to(['/services/default/json']);
$initScript = <<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
$initScriptService = <<< SCRIPT
function (element, callback) {
    var id=\$('#reserve-room_id').val();
    if (id !== "") {
        \$.ajax("{$urlService}?room_id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
?>

<div class="reserve-form">
    <?php $form = ActiveForm::begin([
    ]); ?>
    <?= $form->field($model, 'room_id')->dropDownList(yii\helpers\ArrayHelper::map(\app\modules\rooms\models\Room::getForList(), 'id', 'title')) ?>

    <div class="row">
        <div class="col-xs-10">
                <?= $form->field($model, 'band_id')->widget(Select2::className(), [
                    'data' => yii\helpers\ArrayHelper::map(\app\modules\bands\models\Band::find()->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Укажите название группы ...'],
                    'model' => $model,
                    'attribute' => 'band_id',
                    'value' => isset($model->band->title) ? $model->band->title : null,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(term,page) { return term; }'),
                            'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                        ],
                        'initSelection' => new JsExpression($initScript)
                    ],
                ]); ?>
        </div>
        <div class="col-xs-2">
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <a href="/bands/default/create" class="btn btn-default btn-block" title="Новый клиент" data-async="1"
                   data-container="modal"><?= \kartik\icons\Icon::show('group'); ?></a>
            </div>
        </div>
    </div>

    <?
    $nightOptions = [];
    if ($model->is_night) {
        $nightOptions['disabled'] = 'disabled';
    }
    ?>

    <?= $form->field($model, 'hours')->widget(TouchSpin::classname(), [
        'pluginOptions' => ['min' => 1, 'max' => 16],
        'options' => $nightOptions

    ]); ?>

    <?= $form->field($model, 'start_hour')->widget(TouchSpin::classname(), [
        'pluginOptions' => ['min' => 0, 'max' => 23],
        'options' => $nightOptions
    ]); ?>

    <?= $form->field($model, 'is_night')->checkbox(); ?>

    <? if (Yii::$app->user->can('createPayment')): ?>
        <?= $form->field($model, 'discount_id')->dropDownList(yii\helpers\ArrayHelper::map(\app\modules\payments\models\Discount::find()->all(), 'id', 'title'), ['prompt' => 'Укажите скидку']) ?>
    <? endif ?>

    <div class="row">
        <div class="col-xs-10">
            <?= $form->field($model, 'new_services')->widget(Select2::className(),
                [
                    'data' => yii\helpers\ArrayHelper::map(\app\modules\services\models\Service::getForList(), 'id', 'title'),
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'options' => [
                        'placeholder' => 'Укажите доп. услуги',
                        'class' => 'form-control',
                        'multiple' => true
                    ],
                ]) ?>
        </div>
        <div class="col-xs-2">
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <a href="/services/default/list" class="btn btn-default btn-block" title="Добаить доп. услугу"
                   data-async="1" data-container="modal"><?= \kartik\icons\Icon::show('puzzle-piece'); ?></a>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'start')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Укажите дату ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>

    <? if (Yii::$app->user->can('createPayment')): ?>
        <?= $form->field($model, 'end')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Постоянка до ...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>
    <? endif ?>


    <? if (!$model->isNewRecord): ?>
        <?= $form->field($model, 'summary_price')->textInput([
            'disabled' => 'disabled'
        ]) ?>
        <?= $form->field($model, 'summary_payment')->textInput([
            'disabled' => 'disabled'
        ]) ?>
    <? endif; ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a href="/" class="btn btn-danger">Отмена</a>
    </div>

    <?php ActiveForm::end(); ?>
</div>