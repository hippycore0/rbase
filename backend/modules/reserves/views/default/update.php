<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\reserves\models\Reserve */

$this->title = 'Редактирование резерва ' . '№' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Резервы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '№'.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="reserve-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
