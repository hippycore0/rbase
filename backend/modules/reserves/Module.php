<?php

namespace app\modules\reserves;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\reserves\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
