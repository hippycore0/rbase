<?php

namespace app\models;

use app\modules\reserves\models\Reserve;
use app\modules\rooms\models\Room;
use app\modules\bands\models\Band;
use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property integer $room_id
 * @property integer $reserve_id
 * @property integer $band_id
 * @property integer $timestamp
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'room_id', 'reserve_id', 'band_id', 'timestamp'], 'integer']
        ];
    }

    public function getReserve()
    {
        return $this->hasOne(Reserve::className(), ['id' => 'reserve_id']);
    }

    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    public function getBand()
    {
        return $this->hasOne(Band::className(), ['id' => 'band_id']);
    }

    public function getBrothers(){
        $dayStart = strtotime(date('d.m.Y',$this->timestamp));
        $dayEnd = $dayStart+24*60*60;
        return Schedule::find()->where('timestamp >= :dayStart AND timestamp < :dayEnd AND reserve_id = :reserveID',[':dayStart'=>$dayStart,':dayEnd'=>$dayEnd,':reserveID'=>$this->reserve_id])
            ->orderBy(['timestamp'=>SORT_ASC]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'reserve_id' => 'Reserve ID',
            'band_id' => 'Band ID',
            'timestamp' => 'Timestamp',
        ];
    }
}
