<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_session".
 *
 * @property string $session_id
 * @property string $putdate
 * @property integer $user_id
 */
class UserSession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session_id', 'user_id','putdate'], 'required'],
            [['session_id'], 'string'],
            [['user_id','putdate'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'session_id' => 'Session ID',
            'putdate' => 'Putdate',
            'user_id' => 'User ID',
        ];
    }


    public static function initSession(){
        if(!Yii::$app->user->isGuest){
            $query = UserSession::find()->where(['session_id'=>\Yii::$app->session->getId()]);
            if($query->count() > 0){
                $session = $query->one();
                $session->putdate = time();
                $session->update();
            }else{
                $session = new UserSession();
                $session->session_id = \Yii::$app->session->getId();
                $session->putdate = time();
                $session->user_id = Yii::$app->user->getId();
                $session->save();
            }
        }
        UserSession::deleteAll(['<','putdate',time()-1*60*20]);
    }
}
