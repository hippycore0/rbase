<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $create_at
 * @property integer $user_id
 * @property string $module
 * @property string $controller
 * @property string $action
 */
class History extends \yii\db\ActiveRecord
{

    public $path;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_at', 'user_id','item_id'], 'integer'],
            [['module', 'controller', 'action','comment'], 'string', 'max' => 255]
        ];
    }


    public function afterFind(){
        $this->create_at = date('H:i d.m.Y',$this->create_at);
        $d = explode("\\",$this->module);
        $this->path = '/'.$d[2].'/'.$this->controller.'/'.'view?id='.$this->item_id;
        switch($this->action){
            case 'create':
                $this->action =  'создал';
                break;
            case 'delete':
                $this->action =  'стер';
                break;
            case 'day':
                $this->action =  'отменил день ('.$this->comment.')';
                break;
            case 'remove':
                $this->action =  'удалил';
                break;
            case 'cancel':
                $this->action =  'отменил';
                break;
            case 'update':
                $this->action =  'отредактировал';
                break;
            case 'define':
                $this->action =  'отменил штраф';
                break;
        }
        switch($this->module){
            case 'app\modules\reserves\models\Reserve':
                $this->module =  'резерв';
                break;
            case 'app\modules\payments\models\Payment':
                $this->module =  'оплату';
                break;
            case 'app\modules\bands\models\Band':
                $this->module =  'клиента';
                break;
        }
        parent::afterFind();
        return true;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_at' => 'Create At',
            'user_id' => 'User ID',
            'module' => 'Модуль',
            'controller' => 'Controller',
            'action' => 'Действие',
            'item_id' => 'Объект',
            'comment'=>'Description'
        ];
    }
}
