<?php
namespace backend\models;

use yii\base\Model;
use backend\modules\users\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'validateUser'],
        ];
    }


    public function validateUser($attribute, $params)
    {
        $user = $this->getUser();
        if (!$user)
            $this->addError($attribute, 'Пользователь с таким адресом не существует');
        elseif (!$user->checkResetTokenCount())
            $this->addError($attribute, 'Количество запросов пароля в суки исчерпано. Повторите попытку через 24 часа.');

    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = $this->getUser();

        if ($user) {
            $user->generatePasswordResetToken();
            if ($user->save()) {
                return \Yii::$app->mail->compose('passwordResetToken', ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }

    protected function getUser()
    {
        if (!$this->_user)
            $this->_user = User::findOne([
                'status' => User::STATUS_ACTIVE,
                'email' => $this->email,
            ]);

        return $this->_user;
    }
}
