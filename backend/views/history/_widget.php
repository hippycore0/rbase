<?
use yii\helpers\Html;
?>
<table class="table table-striped table-bordered detail-view">
<?foreach($model as $history):?>
    <tr>
        <td><?=Html::a(\backend\modules\users\models\User::findOne($history->user_id)->username,['/users/manage/view','id'=>$history->user_id]);?></td>
        <td><?=$history->action?></td>
        <td><?=Html::a($history->module.' №'.$history->item_id,[$history->path])?></td>
        <td>в <?=$history->create_at?></td>
    </tr>
<?endforeach?>
</table>
