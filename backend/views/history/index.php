<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="history-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'user_id',
                'format' => 'html',
                'label' => 'Пользователь',
                'value' => function ($model) {
                    $user = \backend\modules\users\models\User::findOne($model->user_id);
                    if($user){
                        return Html::a(\backend\modules\users\models\User::findOne($model->user_id)->username,['/users/manage/view','id'=>$model->user_id]);
                    }else{
                        return $model->user_id;
                    }

                }
            ],
            'action',
            'module',
            [
                'attribute'=>'item_id',
                'label'=>'№'
            ],
            [
                'attribute'=>'create_at',
                'label'=>'время'
            ],
            [
                'attribute'=>'comment',
                'label'=>'Комментарий'
            ],

        ],
    ]); ?>

</div>
