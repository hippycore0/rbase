<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Modal;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=800, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: rBase</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= $this->render('_topnav')?>
    <div class="" id="content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?if(\Yii::$app->controller->action->id == 'create' || \Yii::$app->controller->action->id == 'update'): ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <?if(Yii::$app->session->hasFlash('error')):?>
                        <div class="alert alert-danger">
                            <?=Yii::$app->session->getFlash('error');?>
                        </div>
                    <?endif;?>
                    <?if(Yii::$app->session->hasFlash('success')):?>
                        <div class="alert alert-success">
                            <?=Yii::$app->session->getFlash('success');?>
                        </div>
                    <?endif;?>
                    <?= $content ?>
                </div>
            </div>
        <? else:?>
            <div id="schedule">
                <?if(Yii::$app->session->hasFlash('error')):?>
                    <div class="alert alert-danger">
                        <?=Yii::$app->session->getFlash('error');?>
                    </div>
                <?endif;?>
                <?if(Yii::$app->session->hasFlash('success')):?>
                    <div class="alert alert-success">
                        <?=Yii::$app->session->getFlash('success');?>
                    </div>
                <?endif;?>
            <?= $content ?>
            </div>
            <? if (!Yii::$app->request->isAjax): ?>
                <?=$this->render('@app/views/schedule/_filter');?>
            <? endif; ?>
        <?endif;?>
    </div>
<?
Modal::begin([
    'id' => 'modal',
    'size' => 'modal-md',
    'header' => '&nbsp;'
]);
Modal::end();
?>
<div id="loader" class="loader">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
