<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use kartik\icons\Icon;
Icon::map($this);
NavBar::begin([
    'brandLabel' => Icon::show('cubes').'rBase',
    'brandUrl' => Yii::$app->homeUrl,
    'brandOptions' => [
      'class'=>'hidden-sm'
    ],
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
]);
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
} else {
    /*Основное меню*/
    $menuItems = [
        ['label' => Icon::show('calendar').'Новый резерв', 'url' => ['/reserves/default/create']],
        ['label' => Icon::show('home').'Расписание', 'url' => ['/schedule']],
        ['label' => Icon::show('list').'Справочники', 'items'=>[
            Yii::$app->user->can('edit')?['label' => Icon::show('key').'Услуги', 'url' => ['/rooms']]:'',
            Yii::$app->user->can('edit')?['label' => Icon::show('sitemap').'Комлпексы', 'url' => ['/rooms/bases']]:'',
            Yii::$app->user->can('createClient')?['label' => Icon::show('group').'Клиенты', 'url' => ['/bands']]:'',
            Yii::$app->user->can('underCall')?['label' => Icon::show('puzzle-piece').'Доп. услуги', 'url' => ['/services']]:'',
            Yii::$app->user->can('createReserve')?['label' => Icon::show('calendar').'Резервы','url' => ['/reserves']]:'',
            Yii::$app->user->can('editPayment')?['label' => Icon::show('credit-card').'Оплаты', 'url' => ['/payments']]:'',
            Yii::$app->user->can('editPayment')?['label'=>  Icon::show('money').'Скидки','url'=>'/payments/discount']:'',
            Yii::$app->user->can('all')?['label' => Icon::show('line-chart').'Отчетность', 'url' => ['/report']]:'',
        ]],
        Yii::$app->user->can('all')?['label' => Icon::show('gear').'Настройки','items'=>[
            Yii::$app->user->can('all')?['label' => Icon::show('user').'Пользователи', 'url' => ['/users']]:'',
            Yii::$app->user->can('all')?['label' => Icon::show('edit').'Реквизиты', 'url' => ['/details']]:'',
            Yii::$app->user->can('all')?['label' => Icon::show('calendar').'История', 'url' => ['/history']]:'',
        ]]:'',

        '<li class="divider-vertical"></li>',
    ];
    $menuItems[] = [
        'label' => Icon::show('power-off').'Выход (' . Yii::$app->user->identity->username . ')',
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post']
    ];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right', 'id'=>'topNav'],
    'items' => $menuItems,
    'encodeLabels' => false
]);
NavBar::end();