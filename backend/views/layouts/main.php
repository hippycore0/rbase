<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: rBase</title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">

        <?= $this->render('_topnav')?>

        <div class="container-fluid" id="content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?if(\Yii::$app->controller->action->id != 'index'): ?>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <?if(Yii::$app->session->hasFlash('error')):?>
                    <div class="alert alert-danger">
                        <?=Yii::$app->session->getFlash('error');?>
                    </div>
                <?endif;?>
                <?if(Yii::$app->session->hasFlash('success')):?>
                    <div class="alert alert-success">
                        <?=Yii::$app->session->getFlash('success');?>
                    </div>
                <?endif;?>
                <?= $content ?>
            </div>
        </div>
        <? else:?>
            <?if(Yii::$app->session->hasFlash('error')):?>
                <div class="alert alert-danger">
                    <?=Yii::$app->session->getFlash('error');?>
                </div>
            <?endif;?>
            <?if(Yii::$app->session->hasFlash('success')):?>
                <div class="alert alert-success">
                    <?=Yii::$app->session->getFlash('success');?>
                </div>
            <?endif;?>
        <?= $content ?>
        <?endif;?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left"><?=date('d.m.Y H:i e O')?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
