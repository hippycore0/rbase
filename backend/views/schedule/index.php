<?php
/* @var $this yii\web\View */
use common\components\Days;
use app\modules\reserves\models\Reserve;
$this->title = 'Расписание';
\backend\assets\ScheduleAsset::register($this);
?>

<table class="table table-bordered schedule-body">
    <thead>
    <tr>
        <th class="date">Дата</th>
        <th class="title">Комнаты</th>
        <th class="hour night">Ночь</th>
        <? for ($i = 9; $i <= 23; $i++): ?>
            <th class="hour <?=$i%3==0?'setStart':''?>"><?= $i ?>:00</th>
        <? endfor; ?>
    </tr>
    </thead>
    <tbody>
    <? for ($d = 0; $d <= ($long - 1); $d++): ?>
        <? $d > 0 ? $startDate->modify('+1 day') : ''; ?>
        <? foreach ($rooms as $k => $room): ?>
            <tr<?= $room == end($rooms) ? ' class="last"' : '' ?>>
                <? if ($k == 0): ?>
                    <td class="date<?=Days::isWeekend($startDate->format('d.m.Y'))?' weekend':''?>" rowspan="<?= count($rooms) ?>">
                        <span class="week-day<?=date('Ymd') == date('Ymd', $startDate->format('U'))?' today':''?>"><?= Days::dayOfWeek($startDate) ?></span>
                        <?= $startDate->format('d.m.Y'); ?>
                    </td>
                <? endif; ?>
                <td class="title"><?= $room->title ?><span class="price"><?=$room->price?> руб.</span></td>
                <!-- RESERVES -->
                <?
                $timestamp = strtotime($startDate->format('d.m.Y') . ' ' . 0 . ':00');
                if(isset ($reserves[$timestamp][$room->id])):
                    echo $this->render('_cell',[
                        'model'=>Reserve::findOne($reserves[$timestamp][$room->id]['reserve_id']),
                        'startDate'=>$startDate,
                    ]);
                else:?>
                    <td class="hour empty night"
                        data-hour="0"
                        data-date="<?= $startDate->format('d.m.Y'); ?>" data-room="<?= $room->id ?>"
                        data-price="<?= $room->price ?>">
                    </td>
                <?
                endif;
                $activeHours = 0;
                for ($i = 9; $i <= 23; $i++):
                    if ($activeHours > 0):
                        $activeHours--;
                    else:
                        $timestamp = strtotime($startDate->format('d.m.Y') . ' ' . $i . ':00');
                        if (isset ($reserves[$timestamp][$room->id])):
                            $reserve = Reserve::findOne($reserves[$timestamp][$room->id]['reserve_id']);
                            $activeHours = $reserve->hours - 1;
                            echo $this->render('_cell',[
                                'model'=>$reserve,
                                'startDate'=>$startDate,
                                'timestamp'=>$timestamp
                            ]);

                        else: ?>
                            <td class="hour empty <?=$i%3==0?'setStart':''?>"
                                data-hour="<?= $i ?>"
                                data-date="<?= $startDate->format('d.m.Y'); ?>" data-room="<?= $room->id ?>"
                                data-price="<?= $room->price ?>"></td>
                        <?endif; ?>
                    <?endif; ?>
                <? endfor; ?>
            </tr>
        <? endforeach; ?>
    <? endfor; ?>
    </tbody>
</table>
<?
