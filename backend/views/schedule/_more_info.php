<?
/**
 * @var $model app\modules\reserves\models\Reserve
 * @var $schedule app\models\Schedule
 */
use kartik\icons\Icon;
use yii\helpers\Html;
Icon::map($this,Icon::EL);
Icon::map($this,Icon::FA);
?>
<?=Icon::show('times',['class'=>'close'],Icon::FA);?>
    <h5><a href="/reserves/default/detail?id=<?=$model->id?>">Резерв №<?=$model->id?></a>
    <?if(Yii::$app->user->can('underCall')):?>
        &nbsp;&nbsp;&nbsp;<a href="/reserves/default/update?id=<?=$model->id?>" class="small"><?=Icon::show('edit')?></a>
    <?endif;?>
    </h5>
    <p><strong>Стоимость:</strong> <?=$model->summary_price?> руб.</p>
<?if(isset($model->discount)):?>
    <p><strong>Скидка:</strong> <?=$model->discount->value?> <?=$model->discount->type == 1?'руб.':'%'?></p>
<?endif;?>
    <p><strong>Оплачено:</strong> <?=$model->summary_payment?> руб.</p>
<?if($model->getWeeks() > 1):?>
    <br />
    <p><strong>Постоянка:</strong> <?=$model->getWeeks()?> недель</p>
    <p>c <?=date('d.m.Y',strtotime($model->start))?> до <?=date('d.m.Y',strtotime($model->end))?></p>
<?endif;?>
    <br />
    <p><strong>Зарезервирован:</strong> <?=date('H:i d.m.Y',$model->created_at)?></p>
    <p><strong>Пользователем:</strong> <?=Html::a(\backend\modules\users\models\User::findOne($model->user_create)->username,['/users/manage/view','id'=>$model->user_create]);?></p>
<?if(isset($model->updated_at)):?>
    <p><strong>Обновлен:</strong> <?=date('H:i d.m.Y',$model->updated_at)?></p>
<?endif;?>
    <br />
    <p><strong>Клиент:</strong> <?=Html::a($model->band->title,['/bands/default/view','id'=>$model->band->id])?></p>
<?if($model->band->debt):?>
    <p class="color-red"><strong>Долг:</strong> <?=$model->band->debt?> руб.</p>
<?endif;?>
<?if($model->band->getFined()->count() > 0):?>
    <?foreach($model->band->getFined()->all() as $reserve):?>
        <p><strong>Штраф:</strong> <a href="<?=$reserve->getUrl()?>">от <?=$reserve->start?> <?=$reserve->room->title?> <?=$reserve->start_hour?>:00 <?=$reserve->calculateFine()?> р.</a></p>
    <?endforeach;?>
<?endif;?>
<?if(isset($model->services)&&count($model->services)>0):?>
    <br />
    <h5>Доп. услуги</h5>
    <table class="table">
        <?foreach($model->services as $service):?>
            <tr><td><strong><?=$service->title?>:</strong></td><td class="col-xs-4"><?=$service->price?> <?=$service->units?></td></tr>
        <?endforeach;?>
    </table>
<?endif;?>
    <hr />
    <div class="row">
        <?if(Yii::$app->user->can('underCall')):?>
            <div class="col-xs-6">
                <?= $model->needPay() > 0 ? Html::a('Оплатить', ['/reserves/default/payment', 'id' => $model->id], ['class' => 'btn btn-success btn-sm']):'' ?>
            </div>
        <?endif;?>
        <div class="col-xs-6 text-right">
        <?if($model->getWeeks() > 1):?>

            <?= Html::a('Удалить', ['/reserves/default/day', 'id' => $schedule->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Уверены что хотите удалить день из постоянки?',
                    'method' => 'post',
                ],
            ]) ?>
        <?else:?>
            <?= Html::a('Удалить', ['/reserves/default/remove', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Уверены что хотите удалить позицию?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
        </div>
    </div>
<?if($model->isActual()):?>
    <br />
    <div class="row">
        <div class="col-xs-12">
        <?if($model->getWeeks() > 1):?>
            <?=Html::a('Отменить бронь', ['/reserves/default/daycancel', 'id' => $schedule->id], ['class' => 'btn btn-warning  btn-sm btn-block', 'data'=> ['confirm'=>'Если резерв будет отменен менее чем за 48 часов. Штраф +50%: '.$model->calculateFine().' руб. Действительно отменить?']])?>
        <?else:?>
            <?=Html::a('Отменить бронь', ['/reserves/default/cancel', 'id' => $model->id], ['class' => 'btn btn-warning  btn-sm btn-block', 'data'=> ['confirm'=>'Если резерв будет отменен менее чем за 48 часов. Штраф +50%: '.$model->calculateFine().' руб. Действительно отменить?']])?>
        <?endif;?>
        </div>
    </div>
<?endif;?>