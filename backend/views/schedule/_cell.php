<?
use kartik\icons\Icon;
Icon::map($this,Icon::EL);
Icon::map($this,Icon::FA);
?>

<td
    class="hour reserved<?=$model->isDayDay()?' dayday':''?>"
    title="<?=$model->band->title;?> "
    data-hour="0"
    data-reserve="<?=$model->id;?>"
    data-date="<?=$startDate->format('d.m.Y');?>"
    data-room="<?=$model->room->id;?>"
    data-price="<?=$model->room->price;?>"
    data-band="<?=$model->band->id;?>"
<?if(!$model->is_night):?>
    colspan="<?=$model->hours;?> "
<?endif;?>
    style="background:<?=$model->room->color;?>">
    <a href="<?=Yii::$app->user->can('createReserve')?'/reserves/default/detail?id='.$model->id:'#';?>" class="reserved-cell">
        <span class="band-title">
            <?=$model->band->title;?>
        </span>
        <?if(Yii::$app->user->can('create')):?>
            <?=$model->summary_payment >= $model->summary_price ? Icon::show('rub', ['class' => 'pull-right','title'=>'Оплачено']) : '';?>
            <?=$model->getWeeks() > 1 && (int)date('U',strtotime($model->end)) > (int)$startDate->format('U') ? Icon::show('calendar', ['class' => 'pull-right','title'=>'Постоянка']):'';?>
            <?=$model->getWeeks() > 1 && (int)date('U',strtotime($model->end)) == (int)date('U',strtotime($startDate->format('d.m.Y'))) ? Icon::show('calendar-o', ['class' => 'pull-right','title'=>'Конец постоянки']):'';?>
            <?=$model->band->debt > 0 ? Icon::show('exclamation', ['class' => 'pull-right color-red','title'=>'Долг!']) : '';?>
            <?foreach($model->services as $service):?> 
                <?=$service->icon?Icon::show($service->icon, ['class' => 'pull-right','title'=>$service->title],Icon::EL):'';?>
            <?endforeach;?>
        <?endif;?>
    </a>

    <?if(Yii::$app->user->can('create')):?>
    <div class="more-info">
    </div>
    <?endif;?>
</td>