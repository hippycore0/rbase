<?
use yii\helpers\ArrayHelper;
use app\modules\rooms\models\Room;
use kartik\widgets\Select2;
use yii\web\JsExpression;
$url = \yii\helpers\Url::to(['/bands/default/json']);
$urlService = \yii\helpers\Url::to(['/services/default/json']);
$initScript = <<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
$initScriptService = <<< SCRIPT
function (element, callback) {
    var id=\$('#reserve-room_id').val();
    if (id !== "") {
        \$.ajax("{$urlService}?room_id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
?>
<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" id="scheduleFilter">
    <div class="container-fluid text-right">
        <form class="navbar-form" role="form" action="/schedule" id="filter">
            <div class="form-group">
                <?= \yii\helpers\Html::dropdownList('long', Yii::$app->session->get('schedule_long'), [0 => 'По времени', 1 => 'День', 3 => '3 дня', 7 => 'Неделя', 14 => '2 недели', 30 => 'месяц'], ['class' => 'form-control', 'empty' => 'По времени']) ?>
            </div>
            <div class="form-group">
                <?=
                \yii\jui\DatePicker::widget([
                    'name' => 'date',
                    'language' => 'ru',
                    'value'=>Yii::$app->session->get('schedule_start'),
                    'dateFormat'=> 'dd.MM.yyyy',
                    'options' => ['placeholder' => 'Начальная дата', 'class' => 'form-control', 'size'=>'10']
                ])?>
            </div>
            <div class="form-group">
            <?
                $roomsArray = ArrayHelper::map(Room::find()->all(), 'id', 'title');
                array_unshift($roomsArray, 'Все комнаты');
                echo \yii\helpers\Html::dropdownList('room_id', Yii::$app->session->get('schedule_room_id'), $roomsArray, ['class' => 'form-control', 'empty' => 'По комнате']);
            ?>
                </div>
            <div class="form-group pull-left">
            <?=
            Select2::widget([
                'data' => yii\helpers\ArrayHelper::map(\app\modules\bands\models\Band::find()->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Укажите название группы ...'],
                'name' => 'band_id',
                'value' => Yii::$app->session->has('schedule_band_id')?Yii::$app->session->get('schedule_band_id'):'',
                'pluginOptions' => [
                    'allowClear' => true,
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(term,page) { return {search:term}; }'),
                        'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                    ],
                    'initSelection' => new JsExpression($initScript)
                ],
            ])
            ?>
                </div>
        </form>
    </div>
</nav>