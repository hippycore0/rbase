<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log','gii','backend\components\HistoryHelper'],
    'language' => 'ru-RU',
    'modules' => [
        'users' => 'backend\modules\users\Module',
        'mail' => 'backend\modules\mail\Module',
        'gii' => 'yii\gii\Module',
        'rooms' =>  'app\modules\rooms\Module',
        'bands' =>  'app\modules\bands\Module',
        'reserves' =>  'app\modules\reserves\Module',
        'services' =>  'app\modules\services\Module',
        'payments' =>  'app\modules\payments\Module',
        'details' => 'app\modules\details\Module',
        'report' => 'backend\modules\reports\Module',
        'api' => 'backend\modules\api\Module',
    ],

    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'class' => 'yii\rest\UrlRule',
                'controller' => 'api/rooms',

            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@backend/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'days',
        'user' => [
            'identityClass' => 'backend\modules\users\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',

            //'authFile' => '@console/data/rbac.php', //the default path for rbac.php | OLD CONFIGURATION
            'defaultRoles' => ['root','admin','manager','call','view'],
           // 'itemFile' => '@backend/rbac/items.php',
           // 'ruleFile' => '@backend/rbac/rules.php',
          //  'assignmentFile' => '@backend/rbac/assignments.php',
            /*
             //Default path to items.php | NEW CONFIGURATIONS
            'assignmentFile' => '@console/data/assignments.php', //Default path to assignments.php | NEW CONFIGURATIONS
             //Default path to rules.php | NEW CONFIGURATIONS*/
        ] ,

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>array(
            'cookieValidationKey' => 'mykey',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ),
        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en',
                ],
            ],
        ],
        'assetManager' => array(
            'converter'=>array(
                'class'=>'nizsheanez\assetConverter\Converter',
                'force'=>false, // true : If you want convert your sass each time without time dependency
                'destinationDir' => 'compiled', //at which folder of @webroot put compiled files
                'parsers' => array(
                    'sass' => array( // file extension to parse
                        'class' => 'nizsheanez\assetConverter\Sass',
                        'output' => 'css', // parsed output file type
                        'options' => array(
                            'cachePath' => '@app/runtime/cache/sass-parser' // optional options
                        ),
                    ),
                    'scss' => array( // file extension to parse
                        'class' => 'nizsheanez\assetConverter\Sass',
                        'output' => 'css', // parsed output file type
                        'options' => array() // optional options
                    ),
                    'less' => array( // file extension to parse
                        'class' => 'nizsheanez\assetConverter\Less',
                        'output' => 'css', // parsed output file type
                        'options' => array(
                            'auto' => true, // optional options
                        )
                    )
                )
            )
        ),
    ],
    'params' => $params,
];
