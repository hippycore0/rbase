var Connect = {
    id: 'dialog_'+makeid(),
    pjax: $.pjax,
    a:null,
    dynamicDialog: function(){
        var dialog = $('<div class="dialog" id="'+this.id+'"></div>');
        $('body').append(dialog);
        return dialog;
    },
    get: function(url){
        this.before();
        var container = this.dynamicDialog();
        container.on('pjax:success',function(){
            Connect.after();
        });
        this.pjax({url:url,container:'#'+this.id});
        return false;
    },
    post: function(e){
        this.before();
        this.pjax.submit(e, '#'+this.id);
        return false;
    },
    before:function(){
        $('#loader').fadeIn(100);
    },
    after:function(){
        $('#'+this.id).dialog({
            minWidth: 600,
            maxHeight:600
        }).dialogExtend({
            "closable" : true, // enable/disable close button
            "maximizable" : true, // enable/disable maximize button
            "minimizable" : true, // enable/disable minimize button
            "collapsable" : true, // enable/disable collapse button
        });
        $('#loader').fadeOut(100);
    }
};

$(document).on('submit','.ui-dialog form',function(e){
    Connect.post(e);
    return false;
}).on('click','a[data-async="1"]',function(e){
    Connect.get($(this).attr('href'));
    return false;
});