$(function(){
    var hours = $('input[name="Reserve[hours]"]');
    var start = $('input[name="Reserve[start_hour]"]');
    $('.field-reserve-is_night input[type="checkbox"]').on('change',function(){
        if($(this).is(':checked')){
            hours.val('6').attr('value','6').attr('disabled','disabled');
            start.val('0').attr('value','0').attr('disabled','disabled');
        }else{
            hours.val('1').attr('value','1').removeAttr('disabled');
            start.val('8').attr('value','8').removeAttr('disabled');
        }
    });
    $('#reserve-room_id').on('change',function(){
        $('#reserve-new_services').val(null);
    });
});