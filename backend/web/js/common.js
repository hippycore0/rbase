Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

$(document).keyup(function(e) {
    if (e.keyCode == 27) { //esc
        window.location.href =  '/';
    }
});

$(function(){
    $('#service-icon').on('change',function(){
        $('#iconexample').attr('class','el-icon-'+$(this).val());
    });
    $('#flexible_discount_link').on('click',function(){
        $('#flexible_discount').slideToggle();
    });

});
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}