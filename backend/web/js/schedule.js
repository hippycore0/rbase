var reserves = [];
var panel = $('#reserveControl');
var modal = $('#reserveModal');
var sh = 0;
$(function(){
   setTableWidth();
    $(window).on('resize',function(){
    });
    $(window).scroll(function(){
        if($(this).scrollTop() > 0){
           $('.schedule-header').addClass('scrolled');
        }else{
           $('.schedule-header').removeClass('scrolled');
        }
    });
});

$(document).on('click','.reserved-cell',function(){
    var parentCell = $(this).parent('.hour.reserved');

    $.ajax({
        url:'/schedule/cell',
        type: 'GET',
        data: {id:parentCell.data('reserve'),date:parentCell.data('date')},
        success:function(data){
            parentCell.toggleClass('active');
            $('.hour.reserved').not(parentCell).removeClass('active');
            parentCell.find('.more-info').html(data);
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
    return false;
}).on('click','.more-info .close',function(){
    $(this).closest('.hour.reserved').removeClass('active');
}).on('dblclick','.schedule-body .hour.empty',function(e){
    var href =  '/reserves/default/create?hour='+ $(this).attr('data-hour') +'&date=' + $(this).attr('data-date') + '&room=' + $(this).attr('data-room');
    /*Connect.get(href);*/
    window.location.href =  href;
    return false;
}).on('dblclick','.reserved-cell',function(e){
    window.location.href =  $(this).attr('href');
    return false;
});
$('#filter').on('submit',function(){
    var form = $(this);
    $.ajax({
        data: form.serialize(),
        url: form.attr('action'),
        method: 'POST',
        success: function(data){
            $('#schedule').html(data);
            setTableWidth();
        }
    });
    return false;
}).find('select,input').on('change',function(){
    $('#filter').submit();
});

function setTableWidth(){
    var table = $('.schedule-body');
    table.fixMe();
    table.find('tr td.reserved:odd').find('.band-title').addClass('odd');
}

;(function($) {
    $.fn.fixMe = function() {
        return this.each(function() {
            var $this = $(this),
                $t_fixed;
            function init() {
                $this.wrap('<div class="table_container" />');
                $t_fixed = $this.clone();
                $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
                resizeFixed();
            }
            function resizeFixed() {
                $t_fixed.find("th").each(function(index) {
                    $(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
                });
            }
            function scrollFixed() {
                var offset = $(this).scrollTop(),
                    tableOffsetTop = $this.offset().top,
                    tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
                if(offset < tableOffsetTop || offset > tableOffsetBottom)
                    $t_fixed.hide();
                else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
                    $t_fixed.show();
            }
            $(window).resize(resizeFixed);
            $(window).scroll(scrollFixed);
            init();
        });
    };
})(jQuery);