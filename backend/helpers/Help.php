<?php
namespace  app\helpers;

class Help extends \yii\helpers\Inflector
{
    public static function dayOfWeek($date){
        if(is_int($date))
            $date = date('Y-m-d',$date);
        elseif(is_object($date))
            $date = $date->format('Y-m-d');

        return str_replace(array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),array('Пн','Вт','Ср','Чт','Пт','Сб','Вс'),date('D',strtotime($date)));

    }
}