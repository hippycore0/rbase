<?php
namespace backend\components;
use Yii;
use yii\base\BootstrapInterface;
use app\models\History;
class HistoryHelper extends \yii\base\Component implements BootstrapInterface
{
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    public function bootstrap($app)
    {

    }

    public static function saveHistory($params){
        $history = new History;
        $history->item_id = $params['id'];
        $history->action = $params['action'];
        $history->controller = $params['controller'];
        $history->module = $params['module'];
        $history->user_id = $params['user_id'];
        $history->create_at = time('U');
        if(isset($params['comment'])){
            $history->comment = $params['comment'];
        }
        $history->save();
    }
}
