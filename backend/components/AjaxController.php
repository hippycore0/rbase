<?php
namespace app\components;
use Yii;
use yii\web\Controller;

class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function render($view, $params = [])
    {
        if (Yii::$app->request->isAjax) {
            return parent::renderAjax($view, $params);
        } else {
            return parent::render($view, $params);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'frontend\assets\AppAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
            ];
        }
        return parent::beforeAction($action);
    }
}