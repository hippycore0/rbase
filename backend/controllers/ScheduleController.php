<?php

namespace backend\controllers;

use app\models\Schedule;
use Yii;
use app\modules\reserves\models\Reserve;
use app\modules\rooms\models\Room;
use yii\web\ForbiddenHttpException;
use app\components\AjaxController;

class ScheduleController extends AjaxController
{


    public function actionIndex()
    {
        $this->layout = 'schedule';

        if (!Yii::$app->user->can('viewTable')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $params = Yii::$app->request->post();


        if (isset($params['room_id']) && $params['room_id'] > 0):
            Yii::$app->session->set('schedule_room_id', $params['room_id']);
        elseif (isset($params['room_id']) && $params['room_id'] == 0):
            Yii::$app->session->destroy('schedule_room_id');
        endif;
        if (Yii::$app->session->has('schedule_room_id')) {
            $rooms = Room::findAll(Yii::$app->session->get('schedule_room_id'));
        } else {
            $rooms = Room::find()->all();
        }

        if (isset($params['long']) && $params['long'] > 0):
            Yii::$app->session->set('schedule_long', $params['long']);
        endif;
        Yii::$app->session->has('schedule_long') ? $long = Yii::$app->session->get('schedule_long') : $long = 7;

        if (isset($params['date']) && $params['date'] > 0):
            Yii::$app->session->set('schedule_start', $params['date']);
        endif;
        if (Yii::$app->session->has('schedule_start')) {
            $start = new \Datetime(Yii::$app->session->get('schedule_start'));
            $startDate = new \Datetime(Yii::$app->session->get('schedule_start'));
        } else {
            $d = new \Datetime();
            $d->setTime(0, 0, 0);
            $start = $d;
            $startDate = $d;
        }

        $end = new \Datetime($start->format('Y-m-d H:i:s'));
        $end->modify('+' . $long . ' day');

        if (isset($params['band_id']) && $params['band_id'] > 0):
            Yii::$app->session->set('schedule_band_id', $params['band_id']);
        endif;
        $band_id = Yii::$app->session->has('schedule_band_id') ? Yii::$app->session->get('schedule_band_id') : false;


        $params = ['rooms' => $rooms, 'startDate' => $startDate, 'long' => $long, 'start' => $start, 'end' => $end, 'reserves' => Reserve::getActiveReserves($start, $end, $band_id)];
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', $params);
        } else {
            return $this->render('index', $params);
        }
    }

    public function actionCell($id, $date)
    {
        $model = Reserve::findOne($id);
        $scheduleTimestamp = new \DateTime($date . ' ' . $model->start_hour . ':00');
        $schedule = $model->getSchedule()->where(['timestamp' => $scheduleTimestamp->format('U')])->one();
        return $this->renderPartial('_more_info.php', ['model' => $model, 'schedule' => $schedule]);
    }

    public function actionUpdate($offset = 0, $limit = 300)
    {
        $reserves = Reserve::find()->offset($offset)->limit($limit)->all();
        if (count($reserves) > 0) {
            foreach ($reserves as $reserve) {
                $timestamps = json_decode($reserve->data);
                $newdata = [];
                if (is_array($timestamps)) {
                    foreach ($timestamps as $t => $timestamp) {
                        $newdata[$t + 3600] = $timestamp;
                    }
                } else {
                    Yii::$app->session->set('bad_reserves', Yii::$app->session->get('bad_reserves') . ' ' . $reserve->id);
                }
                $reserve->data = json_encode($newdata);
                $reserve->save();
            }
            $this->redirect('/schedule/update?offset=' . ($offset + $limit));
        } else {
            echo 'Ok';
            var_dump(Yii::$app->session->get('bad_reserves'));
        }


    }
}
