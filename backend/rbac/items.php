<?php
return [
    'login' => [
        'type' => 2,
    ],
    'logout' => [
        'type' => 2,
    ],
    'error' => [
        'type' => 2,
    ],
    'sign-up' => [
        'type' => 2,
    ],
    'index' => [
        'type' => 2,
    ],
    'viewTable' => [
        'type' => 2,
    ],
    'createReserve' => [
        'type' => 2,
    ],
    'createClient' => [
        'type' => 2,
    ],
    'createService' => [
        'type' => 2,
    ],
    'create' => [
        'type' => 2
    ],
    'underCall' => [
        'type' => 2,
    ],
    'reserveRecording' => [
        'type' => 2,
    ],
    'createPayment' => [
        'type' => 2,
    ],
    'print' => [
        'type' => 2,
    ],
    'edit' => [
        'type' => 2,
    ],
    'editPayment' => [
        'type' => 2,
    ],
    'all' => [
        'type' => 2,
    ],
    'root' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'editPayment',
            'all',
            'admin',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'edit',
            'manager',
        ],
    ],
    'manager' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'createPayment',
            'print',
            'call',
            'underCall',
            'reserveRecording'
        ],
    ],
    'call' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'createReserve',
            'createClient',
            'createService',
            'create',
            'view',
        ],
    ],
    'view' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'viewTable',
            'guest',
        ],
    ],
    'guest' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'login',
            'logout',
            'error',
            'sign-up',
            'index',
        ],
    ],
];
