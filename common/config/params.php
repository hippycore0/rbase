<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.resetTokensPerDay' => 3,
    'user.loginAttempts' => 5,
];
