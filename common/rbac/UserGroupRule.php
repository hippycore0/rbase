<?php
namespace common\rbac;

use Yii;
use yii\rbac\Rule;
use common\models\User;
use yii\helpers\ArrayHelper;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        //Получаем массив пользователя из базы
        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));

        if ($user) {
            $role = $user->role; //Значение из поля role базы данных
            if ($item->name === 'root') {
                return $role == User::ROLE_ROOT;
            } elseif ($item->name === 'admin') {
                //moder является потомком admin, который получает его права
                return $role == User::ROLE_ROOT || $role == User::ROLE_ADMIN;
            }
            elseif ($item->name === 'manager') {
                return $role == User::ROLE_ROOT || $role == User::ROLE_ADMIN
                || $role == User::ROLE_MANAGER;
            }
            elseif ($item->name === 'call') {
                return $role == User::ROLE_ROOT || $role == User::ROLE_ADMIN
                || $role == User::ROLE_MANAGER || $role == User::ROLE_CALL;
            }
            elseif ($item->name === 'view') {
                return $role == User::ROLE_ROOT || $role == User::ROLE_ADMIN
                || $role == User::ROLE_MANAGER || $role == User::ROLE_CALL
                || $role == User::ROLE_VIEW;
            }
        }
        return false;
    }
}